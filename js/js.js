$(document).ready(function(e)
{
    $('.dropdown-toggle').dropdown();
	
	// prepend upload form with show/hide form text 
	$('#submitassignment').before('<a class="uploadheader" href="#"><i class="icon-plus"></i>Show Upload Form</a>').hide();
	
	// toggle upload form
	$('.uploadheader').live('click', function(e)
	{
		e.preventDefault();
		$('#submitassignment').toggle(200, function(e)
		{
			if($(this).css('display') != 'none')
			{
				$('.uploadheader').html('<i class="icon-minus"></i>Hide Upload Form');
			}
			else
			{
				$('.uploadheader').html('<i class="icon-plus"></i>Show Upload Form');
			}
		});
	});
	
	// if there are errors with the upload form, don't auto hide it!
	// use length to determine if the element exists, $() ALWAYS returns an object
	if($('div[class="alert alert-error"]').length > 0)
	{
		// trigger the click event to change the .uploadheader text and show the form
		// without too much effort
		$('.uploadheader').trigger('click');
	}
	
	// date picker JS
	$(':date').dateinput(
	{
		format: 'dd-mm-yyyy'
	});
	
	// UI dialog
	$('a[data-rel="dialog"]').click(function(e)
	{
		e.preventDefault();
		
		var dialog = $('<div></div>')
						.load($(this).attr('href') + ' div[data-role="dialog"]')
						.dialog(
						{
							autoOpen:false,
							title:'Mark Assignment',
							modal:true,
							position: 'top'
						})
						.dialog('open');
	});
});