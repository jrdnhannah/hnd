<?php if( ! defined('BASEPATH') ) exit('No direct script access!');

interface IUser
{
	// constructor
	public function __construct();
	
	// main methods
	public function LogIn($username, $password);
	public function is_valid_id($id);
	public function is_valid_account($id, $pass);
	public function is_logged_in();
}


/* End of File */
/* Location: ./application/libraries/User.php */