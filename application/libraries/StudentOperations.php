<?php if( ! defined('BASEPATH') ) exit('No direct script access!');

require_once('IUserOperations.php');

class StudentOperations implements IUser
{
	protected $ci;
	
	public function __construct()
	{
		$this->ci =& get_instance();
		$this->ci->load->model('student_userdb');
		$this->ci->load->model('assignments');
	}
	
	
	#########################################
	#	Log In student						#
	#	public								#
	#########################################
	
	public function LogIn($username, $pass)
	{
		if($this->is_valid_account($username, $pass))
		{
			$data = array();
			$data['id'] = $username;
			$data['pass'] = $pass;
			$data['firstname'] = $this->ci->student_userdb->get_first_name($username);
			$data['lastname'] = $this->ci->student_userdb->get_last_name($username);
			$data['course'] = $this->ci->student_userdb->get_course_id($username);
			$data['year']	= $this->ci->student_userdb->get_year($username);
			
			$this->ci->load->library('Student', $data);
			
			setcookie('userdata', base64_encode(serialize($this->ci->student)), 0, '/', '.hnd.wp7solutions.co.uk');
			
			header('Location: ./StudentPortal.htm');
		}
	}
	
	
	#########################################
	#	Check if user id is valid			#
	#	public								#
	#########################################
	
	public function is_valid_id($id)
	{
		if($this->ci->student_userdb->is_valid_id($id))
			return true;
		else
			return false;
	}
	
	#########################################
	#	Check if user id + pass is valid	#
	#	public								#
	#########################################
	
	public function is_valid_account($id, $pass)
	{
		if($this->ci->student_userdb->is_valid_account($id, $pass))
			return true;
		else
			return false;
	}
	
	#########################################
	#	Check if user is logged in			#
	#	public								#
	#########################################
	
	public function is_logged_in()
	{
		// require student.php, even though it's loaded by CodeIgniter php still doesn't know about it
		// and we need the student object to work out if the cookie is an instance of it.
		require_once('Student.php');
		
		if(isset($_COOKIE['userdata']) && unserialize(base64_decode($_COOKIE['userdata'])) instanceof Student == true)
		{
			return true;
		}
		else
			return false;
	}
	
	#########################################
	#	Get course name						#
	#	public								#
	#########################################
	
	public function get_course_name($cid)
	{
		return $this->ci->student_userdb->get_course_name($cid);	
	}
	
	#########################################
	#	Get list of modules					#
	#	public								#
	#########################################
	
	public function get_modules($id)
	{
		$this->ci->load->model('modules');
		return $this->ci->modules->get_module_list($id);
	}
	
	#########################################
	#	Get list of assignments for module	#
	#	public								#
	#########################################
	
	public function get_assignments($cid, $mid)
	{
		return $this->ci->assignments->get_assignments($cid, $mid);
	}
	
	#########################################
	#	Get assignment brief				#
	#	public								#
	#########################################
	
	public function get_assignment_brief($aid)
	{
		return $this->ci->assignments->get_assignment_brief($aid);
	}
	
	#########################################
	#	Get valid file types for assignment	#
	#	public								#
	#########################################
	
	public function get_valid_file_types($aid)
	{
		return $this->ci->assignments->get_valid_file_types($aid);
	}
	
	#########################################
	#	Get valid file types for assignment	#
	#	public								#
	#########################################
	
	public function is_valid_assignment_id($aid)
	{
		return $this->ci->assignments->is_valid_id($aid);
	}
	
	#########################################
	#	Is the current assignment revision	#
	#	final?								#
	#	public								#
	#########################################
	
	public function is_current_revision_final(Student $student, $aid)
	{
		return $this->ci->assignments->is_current_revision_final($student, $aid);
	}
	
	#########################################
	#	Insert uploaded submission into db	#
	#	public								#
	#########################################
	
	public function submit_assignment(Student $student, $location)
	{
		return $this->ci->assignments->submit_assignment($student, $location);
	}
	
	#########################################
	#	Get current revision of assignment	#
	#	public								#
	#########################################
	
	public function get_current_revision(Student $student, $aid)
	{
		return $this->ci->assignments->get_current_revision($student, $aid);
	}
	
	#########################################
	#	Get marks							#
	#	public								#
	#########################################
	
	public function get_marks($id)
	{
		return $this->ci->student_userdb->get_marks($id);
	}
}


/* End of File */
/* Location: ./application/libraries/StudentOperations.php */