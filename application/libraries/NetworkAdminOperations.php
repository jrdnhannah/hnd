<?php if( ! defined('BASEPATH') ) exit('No direct script access!');

require_once('IUserOperations.php');

class NetworkAdminOperations implements IUser
{
	private $ci;

	public function __construct()
	{
		$this->ci =& get_instance();
		$this->ci->load->model('nwa_userdb');
	}
	
	#########################################
	#	Log In admin						#
	#	public								#
	#########################################
	
	public function LogIn($username, $pass)
	{
		if($this->ci->nwa_userdb->is_valid_account($username, $pass))
		{
			$data = array();
			$data['id'] = $username;
			$data['pass'] = $pass;
			$data['firstname'] = $this->ci->nwa_userdb->get_first_name($username);
			$data['lastname'] = $this->ci->nwa_userdb->get_last_name($username);
			
			$this->ci->load->library('NetworkAdmin', $data);
			
			setcookie('userdata', base64_encode(serialize($this->ci->networkadmin)), 0, '/', '.hnd.wp7solutions.co.uk');
			
			header('Location: ./NetworkAdminPortal.htm');
		}
	}
	
	#########################################
	#	Check if user id is valid			#
	#	public								#
	#########################################
	
	public function is_valid_id($id)
	{
		if($this->ci->nwa_userdb->is_valid_id($id))
			return true;
		else
			return false;
	}
	
	#########################################
	#	Check if user id + pass is valid	#
	#	public								#
	#########################################
	
	public function is_valid_account($id, $pass)
	{
		if($this->ci->nwa_userdb->is_valid_account($id, $pass))
			return true;
		else
			return false;
	}
	
	#########################################
	#	Check if user is logged in			#
	#	public								#
	#########################################
	
	public function is_logged_in()
	{
		// require staff.php, even though it's loaded by CodeIgniter php still doesn't know about it
		// and we need the student object to work out if the cookie is an instance of it.
		require_once('NetworkAdmin.php');
		
		if(isset($_COOKIE['userdata']) && unserialize(base64_decode($_COOKIE['userdata'])) instanceof NetworkAdmin == true)
		{
			return true;
		}
		else
			return false;
	}
}


/* End of File */
/* Location: ./application/libraries/NetworkAdminOperations.php */