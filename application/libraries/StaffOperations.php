<?php if( ! defined('BASEPATH') ) exit('No direct script access!');

require_once('IUserOperations.php');

class StaffOperations implements IUser
{
	private $ci;

	public function __construct()
	{
		$this->ci =& get_instance();
		$this->ci->load->model('staff_userdb');
		$this->ci->load->model('assignments');
	}
	
	#########################################
	#	Log In student						#
	#	public								#
	#########################################
	
	public function LogIn($username, $pass)
	{
		if($this->is_valid_account($username, $pass))
		{
			$data = array();
			$data['id'] = $username;
			$data['pass'] = $pass;
			$data['firstname'] = $this->ci->staff_userdb->get_first_name($username);
			$data['lastname'] = $this->ci->staff_userdb->get_last_name($username);
			$data['department'] = $this->ci->staff_userdb->get_dept($username);
			
			$this->ci->load->library('Staff', $data);
			
			setcookie('userdata', base64_encode(serialize($this->ci->staff)), 0, '/', '.hnd.wp7solutions.co.uk');
			
			header('Location: ./StaffPortal.htm');
		}
	}
	
	#########################################
	#	Check if user id is valid			#
	#	public								#
	#########################################
	
	public function is_valid_id($id)
	{
		if($this->ci->staff_userdb->is_valid_id($id))
			return true;
		else
			return false;
	}
	
	#########################################
	#	Check if user id + pass is valid	#
	#	public								#
	#########################################
	
	public function is_valid_account($id, $pass)
	{
		if($this->ci->staff_userdb->is_valid_account($id, $pass))
			return true;
		else
			return false;
	}
	
	#########################################
	#	Check if user is logged in			#
	#	public								#
	#########################################
	
	public function is_logged_in()
	{
		// require staff.php, even though it's loaded by CodeIgniter php still doesn't know about it
		// and we need the student object to work out if the cookie is an instance of it.
		require_once('Staff.php');
		
		if(isset($_COOKIE['userdata']) && unserialize(base64_decode($_COOKIE['userdata'])) instanceof Staff == true)
		{
			return true;
		}
		else
			return false;
	}
	
	#########################################
	#	Get dept name						#
	#	public								#
	#########################################
	
	public function get_dept_name($did)
	{
		return $this->ci->staff_userdb->get_dept_name($did);	
	}
	
	#########################################
	#	Get list of taught modules			#
	#	public								#
	#########################################
	
	public function get_modules($id)
	{
		$this->ci->load->model('modules');
		return $this->ci->modules->get_staff_modules($id);
	}
	
	#########################################
	#	Get list of assignments for module	#
	#	public								#
	#########################################
	
	public function get_assignments($mid, $cid, $sid)
	{
		return $this->ci->assignments->get_staff_assignments($mid, $cid, $sid);
	}
	
	#########################################
	#	Insert uploaded brief into db		#
	#	public								#
	#########################################
	
	public function add_assignment($file, $mid)
	{
		return $this->ci->assignments->add_assignment($file, $mid);
	}
	
	#########################################
	#	Get submitted assignments			#
	#	public								#
	#########################################
	
	public function get_submitted_assignments($cid, $mid, $aid)
	{
		return $this->ci->assignments->get_submitted_assignments($cid, $mid, $aid);
	}
	
	#########################################
	#	Get assignment name					#
	#	public								#
	#########################################
	
	public function get_assignment_name($aid)
	{
		return $this->ci->assignments->get_assignment_name($aid);
	}
	
	#########################################
	#	Get assignment due date				#
	#	public								#
	#########################################
	
	public function get_assignment_duedate($aid)
	{
		return $this->ci->assignments->get_assignment_duedate($aid);
	}
	
	#########################################
	#	Get course type of student			#
	#	public								#
	#########################################
	
	public function get_course_type($id)
	{
		return $this->ci->staff_userdb->get_course_type($id);
	}
	
	#########################################
	#	Get grades for course type			#
	#	public								#
	#########################################
	
	public function get_course_marks($ctype)
	{
		return $this->ci->staff_userdb->get_course_marks($ctype);
	}
	
	#########################################
	#	Get student full name				#
	#	public								#
	#########################################
	
	public function get_student_name($pnumber)
	{
		$this->ci->load->model('student_userdb');
		
		return $this->ci->student_userdb->get_full_name($pnumber);	
	}
	
	#########################################
	#	Get assignment submission ID		#
	#	public								#
	#########################################
	
	public function get_assignment_submission_id($sid,$aid)
	{
		return $this->ci->assignments->get_assignment_submission_id($sid,$aid);	
	}
	
	#########################################
	#	Mark Assignment						#
	#	Public								#
	#########################################
	
	public function mark_assignment($sid, $aid, $said, $grade, $percentage, $feedback)
	{
		if($this->ci->assignments->mark_assignment($sid,$aid,$said,$grade,$percentage,$feedback))
			return true;
		else
			return false;
	}
	
	#########################################
	#	Is assignment marked?				#
	#	Public								#
	#########################################
	
	public function is_marked($sid, $aid)
	{
		if($this->ci->assignments->is_marked($sid, $aid))
			return true;
		else
			return false;
	}

}


/* End of File */
/* Location: ./application/libraries/Student.php */