<?php if( ! defined('BASEPATH') ) exit ('No direct script access!');

class Student
{
	public $data = array();
	
	public function __construct($params)
	{
		$this->data['id']			= $params['id'];
		$this->data['pass']			= $params['pass'];
		$this->data['firstname']	= $params['firstname'];
		$this->data['lastname']		= $params['lastname'];
		$this->data['course']		= $params['course'];
		$this->data['year']			= $params['year'];
		
		
	}
	
}

/* End of File */
/* Location: ./application/libraries/Student.php */