<div class="row">
<div class="span6 alert alert-error">
	<strong>Warning!</strong>
    <p>Deleting a department will erase the department, all courses, all modules, assignments, and students' assignment submissions within that department. This will be <strong>unrecoverable</strong>. Please make sure that you are confident in your decision before you delete a module.</p>
</div>

<?=$table?>
</div>