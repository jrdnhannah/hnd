<ul class="nav nav-list span2">
	<li class="nav-header">Misc</li>
    <li><a href="<?=site_url()?>NetworkAdminPortal.htm">Portal Home</a></li>
    <li><a href="<?=site_url()?>Home/LogOut.htm">Log Out</a></li>

	<li class="nav-header">Personnel</li>
   	<li><a href="<?=site_url()?>NetworkAdminPortal/AddStudent.htm">Add Student</a></li>
    <li><a href="<?=site_url()?>NetworkAdminPortal/AddStaff.htm">Add Staff</a></li>
	<li><a href="<?=site_url()?>NetworkAdminPortal/BrowseStudents.htm">Browse Students</a></li>
    <li><a href="<?=site_url()?>NetworkAdminPortal/BrowseStaff.htm">Browse Staff</a></li>
    
    <li class="nav-header">Courses</li>
    <li><a href="<?=site_url()?>NetworkAdminPortal/DeleteAssignmentSubmissions.htm">Delete Assignment Submissions</a></li>
    <li><a href="<?=site_url()?>NetworkAdminPortal/AddModule.htm">Add Module</a></li>
    <li><a href="<?=site_url()?>NetworkAdminPortal/AddCourse.htm">Add Course</a></li>
    <li><a href="<?=site_url()?>NetworkAdminPortal/AddCourseType.htm">Add Course Type</a></li>
    <li><a href="<?=site_url()?>NetworkAdminPortal/AddDepartment.htm">Add Department</a></li>
    <li><a href="<?=site_url()?>NetworkAdminPortal/BrowseModules.htm">Browse Modules</a></li>
	<li><a href="<?=site_url()?>NetworkAdminPortal/BrowseCourses.htm">Browse Courses</a></li>
    <li><a href="<?=site_url()?>NetworkAdminPortal/BrowseCourseTypes.htm">Browse Course Types</a></li>
    <li><a href="<?=site_url()?>NetworkAdminPortal/BrowseDepartments.htm">Browse Departments</a></li>
        
    <li class="nav-header">Database</li>
    <li><a href="http://pma.wp7solutions.co.uk" target="_blank">Access Database</a></li>
</ul>