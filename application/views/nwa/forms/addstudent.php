<?=validation_errors()?>

<?=form_open(current_url())?>

<?=form_fieldset('', array('class' => 'form-horizontal'))?>

<div class="control-group">
<?=form_label('PNumber:', 'pnumber', array('class' => 'control-label'))?>
<div class="controls">
<?=form_input(array('name' => 'pnumber', 'id' => 'pnumber', 'required' => 'required', 'placeholder' => 'Pnumber', 'value' => set_value('pnumber')))?>
</div>
</div>

<div class="control-group">
<?=form_label('First Name:', 'firstname', array('class' => 'control-label'))?>
<div class="controls">
<?=form_input(array('name' => 'firstname', 'id' => 'firstname', 'required' => 'required', 'placeholder' => 'First Name...', 'value' => set_value('firstname')))?>
</div>
</div>

<div class="control-group">
<?=form_label('Last Name:', 'lastname', array('class' => 'control-label'))?>
<div class="controls">
<?=form_input(array('name' => 'lastname', 'id' => 'lastname', 'required' => 'required', 'placeholder' => 'Last Name...', 'value' => set_value('lastname')))?>
</div>
</div>

<div class="control-group">
<?=form_label('Password:', 'password', array('class' => 'control-label'))?>
<div class="controls">
<?=form_password(array('name' => 'password', 'id' => 'password', 'placeholder' => 'Password...', 'required' => 'required'))?>
</div>
</div>

<div class="control-group">
<?=form_label('Confirm Password:', 'password2', array('class' => 'control-label'))?>
<div class="controls">
<?=form_password(array('name' => 'password2', 'id' => 'password2', 'placeholder' => 'Confirm Password...', 'required' => 'required'))?>
</div>
</div>

<div class="control-group">
<?=form_label('Gender:', 'gender', array('class' => 'control-label'))?>
<div class="controls">
	<select name="gender" id="gender" required>
		<option value="" <?=set_select('gender', '')?>></option>
        <option value="Male"<?=set_select('gender', 'Male')?>>Male</option>
        <option value="Female"<?=set_select('gender', 'Female')?>>Female</option>
        <option value="Other"<?=set_select('gender', 'Other')?>>Other</option>
    </select>
</div>
</div>

<div class="control-group">
<?=form_label('Course:', 'course', array('class' => 'control-label'))?>
<div class="controls">
	<select name="course" id="course" required>
    	<option value=""></option>
    	<?php
	
		foreach($courses as $c)
		{
			echo '<option value="'.$c['ID'].'" '.set_select('course', $c['ID']).'>'.$c['Name'].'</option>';
				
			echo "\n";
		}
		?>
    </select>
</div>
</div>

<div class="control-group">
<?=form_label('Email:', 'email', array('class' => 'control-label'))?>
<div class="controls">
<input type="email" name="email" id="email" required="required" placeholder="email" value="<?=set_value('email')?>" />
</div>
</div>

<div class="control-group">
<?=form_label('Address:', 'address', array('class' => 'control-label'))?>
<div class="controls">
<textarea name="address" id="address" rows="5" cols="20"><?=set_value('address')?></textarea>
</div>
</div>

<div class="control-group">
<?=form_label('Start Date:', 'startdate', array('class' => 'control-label'))?>
<div class="controls">
<input type="date" name="startdate" id="startdate" value="<?=set_value('startdate')?>" />
</div>
</div>

<div class="control-group">
<?=form_label('End Date:', 'endate', array('class' => 'control-label'))?>
<div class="controls">
<input type="date" name="enddate" id="enddate" value="<?=set_value('enddate')?>" />
</div>
</div>

<div class="control-group">
<?=form_label('Year:', 'year', array('class' => 'control-label'))?>
<div class="controls">
<input type="number" name="year" id="year" value="<?=set_value('year')?>" />
</div>
</div>

<div class="form-actions">
<?=form_button(array('name' => 'btnSubmit', 'class' => 'btn btn-primary btn-large', 'type' => 'submit', 'content' => 'Add Student'))?>
</div>

<?=form_fieldset_close()?>

<?=form_close()?>