<?=form_open(current_url())?>

<?=form_fieldset('', array('class' => 'form-horizontal'))?>

<?=validation_errors()?>

<div class="control-group">
<?=form_label('First Name:', 'firstname', array('class' => 'control-label'))?>
<div class="controls">
<?=form_input(array('name' => 'firstname', 'id' => 'firstname', 'required' => 'required', 'placeholder' => 'First Name...', 'value' => set_value('firstname', $student->firstName)))?>
</div>
</div>

<div class="control-group">
<?=form_label('Last Name:', 'lastname', array('class' => 'control-label'))?>
<div class="controls">
<?=form_input(array('name' => 'lastname', 'id' => 'lastname', 'required' => 'required', 'placeholder' => 'Last Name...', 'value' => set_value('lastname', $student->lastName)))?>
</div>
</div>

<div class="control-group">
<?=form_label('Reset Password:', 'password', array('class' => 'control-label'))?>
<div class="controls">
<?=form_password(array('name' => 'password', 'id' => 'password', 'placeholder' => 'Reset Password...'))?>
</div>
</div>

<div class="control-group">
<?=form_label('Gender:', 'gender', array('class' => 'control-label'))?>
<div class="controls">
	<select name="gender" id="gender" required>
    	<?php
		$sex = array('', 'Male', 'Female', 'Other');
		
		foreach($sex as $g)
		{
			if($g == $student->gender)
				echo '<option value="'.$g.'" selected="selected">'.$g.'</option>';
			else
				echo '<option value="'.$g.'">'.$g.'</option>';
				
			echo "\n";
		}
		?>
    </select>
</div>
</div>

<div class="control-group">
<?=form_label('Course:', 'course', array('class' => 'control-label'))?>
<div class="controls">
	<select name="course" id="course" required>
    	<option value=""></option>
    	<?php
	
		foreach($courses as $c)
		{
			if($c['ID'] == $student->course_id)
				echo '<option value="'.$c['ID'].'" selected="selected">'.$c['Name'].'</option>';
			else
				echo '<option value="'.$c['ID'].'">'.$c['Name'].'</option>';
				
			echo "\n";
		}
		?>
    </select>
</div>
</div>

<div class="control-group">
<?=form_label('Email:', 'email', array('class' => 'control-label'))?>
<div class="controls">
<input type="email" name="email" id="email" required="required" placeholder="email" value="<?=set_value('email', $student->email)?>" />
</div>
</div>

<div class="control-group">
<?=form_label('Address:', 'address', array('class' => 'control-label'))?>
<div class="controls">
<textarea name="address" id="address" rows="5" cols="20"><?=set_value('address', $student->address)?></textarea>
</div>
</div>

<div class="control-group">
<?=form_label('Start Date:', 'startdate', array('class' => 'control-label'))?>
<div class="controls">
<input type="date" name="startdate" id="startdate" value="<?=set_value('startdate', $student->startdate)?>" />
</div>
</div>

<div class="control-group">
<?=form_label('End Date:', 'endate', array('class' => 'control-label'))?>
<div class="controls">
<input type="date" name="enddate" id="enddate" value="<?=set_value('enddate', $student->enddate)?>" />
</div>
</div>

<div class="form-actions">
<?=form_button(array('name' => 'btnSubmit', 'class' => 'btn btn-primary btn-large', 'type' => 'submit', 'content' => 'Edit Student'))?>
</div>

<?=form_fieldset_close()?>

<?=form_close()?>