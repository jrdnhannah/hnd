<?=validation_errors()?>

<?=form_open(current_url())?>

<?=form_fieldset('', array('class' => 'form-horizontal'))?>

<div class="control-group">
<?=form_label('Staff ID:', 'id', array('class' => 'control-label'))?>
<div class="controls">
<?=form_input(array('name' => 'id', 'id' => 'id', 'required' => 'required', 'placeholder' => 'ID...', 'value' => set_value('id')))?>
</div>
</div>

<div class="control-group">
<?=form_label('First Name:', 'firstname', array('class' => 'control-label'))?>
<div class="controls">
<?=form_input(array('name' => 'firstname', 'id' => 'firstname', 'required' => 'required', 'placeholder' => 'First Name...', 'value' => set_value('firstname')))?>
</div>
</div>

<div class="control-group">
<?=form_label('Last Name:', 'lastname', array('class' => 'control-label'))?>
<div class="controls">
<?=form_input(array('name' => 'lastname', 'id' => 'lastname', 'required' => 'required', 'placeholder' => 'Last Name...', 'value' => set_value('lastname')))?>
</div>
</div>

<div class="control-group">
<?=form_label('Password:', 'password', array('class' => 'control-label'))?>
<div class="controls">
<?=form_password(array('name' => 'password', 'id' => 'password', 'placeholder' => 'Password...', 'required' => 'required'))?>
</div>
</div>

<div class="control-group">
<?=form_label('Confirm Password:', 'password2', array('class' => 'control-label'))?>
<div class="controls">
<?=form_password(array('name' => 'password2', 'id' => 'password2', 'placeholder' => 'Confirm Password...', 'required' => 'required'))?>
</div>
</div>

<div class="control-group">
<?=form_label('Email:', 'email', array('class' => 'control-label'))?>
<div class="controls">
<input type="email" name="email" id="email" required="required" placeholder="Email" value="<?=set_value('email')?>" />
</div>
</div>

<div class="control-group">
<?=form_label('Address:', 'address', array('class' => 'control-label'))?>
<div class="controls">
<textarea name="address" id="address" rows="5" cols="20"><?=set_value('address')?></textarea>
</div>
</div>

<div class="control-group">
<?=form_label('Contact Number:', 'contactnumber', array('class' => 'control-label'))?>
<div class="controls">
<input type="text" name="contactnumber" id="contactnumber" placeholder="Contact Number..." value="<?=set_value('contactnumber')?>" />
</div>
</div>

<div class="control-group">
<?=form_label('Department:', 'department', array('class' => 'control-label'))?>
<div class="controls">
	<select name="department" id="department" required>
    	<option value=""></option>
    	<?php
	
		foreach($departments as $d)
		{
			echo '<option value="'.$d['ID'].'" '.set_select('deparments').'>'.$d['name'].'</option>';
				
			echo "\n";
		}
		?>
    </select>
</div>
</div>

<div class="form-actions">
<?=form_button(array('name' => 'btnSubmit', 'class' => 'btn btn-primary btn-large', 'type' => 'submit', 'content' => 'Add Staff'))?>
</div>

<?=form_fieldset_close()?>

<?=form_close()?>