<?=form_open(current_url())?>

<?=form_fieldset('', array('class' => 'form-horizontal'))?>

<?=validation_errors()?>

<div class="control-group">
<?=form_label('Course Type:', 'coursetype', array('class' => 'control-label'))?>
<div class="controls">
	<select name="coursetype" id="coursetype" required>
    	<option value=""></option>
    	<?php
	
		foreach($coursetypes as $c)
		{
			echo '<option value="'.$c->ID.'">'.$c->type.'</option>';
				
			echo "\n";
		}
		?>
    </select>
</div>
</div>

<div class="control-group">
<?=form_label('Course Name:', 'coursename', array('class' => 'control-label'))?>
<div class="controls">
<?=form_input(array('name' => 'coursename', 'id' => 'coursename', 'required' => 'required', 'placeholder' => 'Course Name...', 'value' => set_value('coursename')))?>
</div>
</div>

<div class="control-group">
<?=form_label('Department:', 'department', array('class' => 'control-label'))?>
<div class="controls">
	<select name="department" id="department" required>
    	<option value=""></option>
    	<?php
	
		foreach($departments as $d)
		{
			echo '<option value="'.$d['ID'].'">'.$d['name'].'</option>';
				
			echo "\n";
		}
		?>
    </select>
</div>
</div>

<div class="control-group">
<?=form_label('Description (Optional):', 'description', array('class' => 'control-label'))?>
<div class="controls">
<textarea name="description" id="description" rows="5" cols="20" placeholder="Course Description..."><?=set_value('description')?></textarea>
</div>
</div>

<div class="form-actions">
<?=form_button(array('name' => 'btnSubmit', 'class' => 'btn btn-primary btn-large', 'type' => 'submit', 'content' => 'Add Course'))?>
</div>

<?=form_fieldset_close()?>

<?=form_close()?>