<?=form_open(current_url())?>

<?=form_fieldset('', array('class' => 'form-horizontal'))?>

<?=validation_errors()?>

<div class="control-group">
<?=form_label('Module Name:', 'modname', array('class' => 'control-label'))?>
<div class="controls">
<?=form_input(array('name' => 'modname', 'id' => 'modname', 'required' => 'required', 'placeholder' => 'Module Name...', 'value' => set_value('modname')))?>
</div>
</div>

<div class="control-group">
<?=form_label('Taught By:', 'taught_by', array('class' => 'control-label'))?>
<div class="controls">
	<select name="taught_by" id="taught_by" required>
    	<option value=""></option>
    	<?php
	
		foreach($staff as $s)
		{
			echo '<option value="'.$s->staff_id.'">'.$s->name.'</option>';
				
			echo "\n";
		}
		?>
    </select>
</div>
</div>

<div class="control-group">
<?=form_label('Course:', 'course', array('class' => 'control-label'))?>
<div class="controls">
	<select name="course" id="course" required>
    	<option value=""></option>
    	<?php
	
		foreach($courses as $c)
		{
			echo '<option value="'.$c['ID'].'">'.$c['Name'].'</option>';
				
			echo "\n";
		}
		?>
    </select>
</div>
</div>

<div class="control-group">
<?=form_label('Year:', 'year', array('class' => 'control-label'))?>
<div class="controls">
<input type="number" name="year" id="year" value="<?=set_value('number')?>" required />
</div>
</div>

<div class="form-actions">
<?=form_button(array('name' => 'btnSubmit', 'class' => 'btn btn-primary btn-large', 'type' => 'submit', 'content' => 'Add Module'))?>
</div>

<?=form_fieldset_close()?>

<?=form_close()?>