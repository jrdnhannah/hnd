<?=form_open(current_url())?>

<?=form_fieldset('', array('class' => 'form-horizontal'))?>

<?=validation_errors()?>

<div class="control-group">
<?=form_label('Course Type:', 'type', array('class' => 'control-label'))?>
<div class="controls">
	<select name="type" id="type" required>
    	<option value=""></option>
    	<?php
	
		foreach($types as $t)
		{
			if($t['type'] == $course->type)
				echo '<option value="'.$t['type'].'" selected="selected">'.$t['type'].'</option>';
			else
				echo '<option value="'.$t['type'].'">'.$t['type'].'</option>';
				
			echo "\n";
		}
		?>
    </select>
</div>
</div>

<div class="control-group">
<?=form_label('Course Name:', 'name', array('class' => 'control-label'))?>
<div class="controls">
<?=form_input(array('name' => 'name', 'id' => 'name', 'required' => 'required', 'placeholder' => 'Course Name...', 'value' => set_value('name', $course->name)))?>
</div>
</div>

<div class="control-group">
<?=form_label('Description (Optional):', 'description', array('class' => 'control-label'))?>
<div class="controls">
<textarea name="description" id="description" rows="5" cols="20" placeholder="Course Description..."><?=set_value('description', $course->description)?></textarea>
</div>
</div>

<div class="control-group">
<?=form_label('Department:', 'department', array('class' => 'control-label'))?>
<div class="controls">
	<select name="department" id="department" required>
    	<option value=""></option>
    	<?php
	
		foreach($depts as $d)
		{
			if($d['ID'] == $course->dept)
				echo '<option value="'.$d['ID'].'" selected="selected">'.$d['name'].'</option>';
			else
				echo '<option value="'.$d['ID'].'">'.$d['name'].'</option>';
				
			echo "\n";
		}
		?>
    </select>
</div>
</div>

<div class="form-actions">
<?=form_button(array('name' => 'btnSubmit', 'class' => 'btn btn-primary btn-large', 'type' => 'submit', 'content' => 'Edit Course'))?>
</div>

<?=form_fieldset_close()?>

<?=form_close()?>