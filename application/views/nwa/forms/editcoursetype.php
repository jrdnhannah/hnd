<?=form_open(current_url())?>

<?=form_fieldset('', array('class' => 'form-horizontal'))?>

<?=validation_errors()?>

<div class="control-group">
<?=form_label('Course Type:', 'type', array('class' => 'control-label'))?>
<div class="controls">
<?=form_input(array('name' => 'type', 'id' => 'type', 'required' => 'required', 'placeholder' => 'Course Type...', 'value' => set_value('type', $course->type)))?>
</div>
</div>

<?php
$course->marks = str_replace(",","\n",$course->marks);
?>

<div class="control-group">
<?=form_label('Course Marks:', 'coursemarks', array('class' => 'control-label'))?>
<div class="controls">
<textarea name="coursemarks" id="coursemarks" rows="5" cols="20" placeholder="Course Types"><?=set_value('coursemarks', $course->marks)?></textarea>
<p class="help-block">Please put each grade on a new line. E.g:<br />
Distinction<br />
Merit<br />
Pass<br />
Fail
</p>
</div>
</div>

<div class="form-actions">
<?=form_button(array('name' => 'btnSubmit', 'class' => 'btn btn-primary btn-large', 'type' => 'submit', 'content' => 'Edit Course'))?>
</div>

<?=form_fieldset_close()?>

<?=form_close()?>