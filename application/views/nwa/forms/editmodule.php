<?=form_open(current_url())?>

<?=form_fieldset('', array('class' => 'form-horizontal'))?>

<?=validation_errors()?>

<div class="control-group">
<?=form_label('Module Name:', 'modname', array('class' => 'control-label'))?>
<div class="controls">
<?=form_input(array('name' => 'modname', 'id' => 'modname', 'required' => 'required', 'placeholder' => 'Module Name...', 'value' => set_value('modname', $module->name)))?>
</div>
</div>

<div class="control-group">
<?=form_label('Course:', 'course', array('class' => 'control-label'))?>
<div class="controls">
	<select name="course" id="course" required>
    	<option value=""></option>
    	<?php
	
		foreach($courselist as $c)
		{
			if($c['ID'] == $current_course)
				echo '<option value="'.$c['ID'].'" selected="selected">'.$c['Name'].'</option>';
			else
				echo '<option value="'.$c['ID'].'">'.$c['Name'].'</option>';
				
			echo "\n";
		}
		?>
    </select>
</div>
</div>

<div class="control-group">
<?=form_label('Lecturer:', 'lecturer', array('class' => 'control-label'))?>
<div class="controls">
	<select name="lecturer" id="lecturer" required>
    	<option value=""></option>
    	<?php
	
		foreach($staff as $s)
		{
			if($s->staff_id == $module->taught_by)
				echo '<option value="'.$s->staff_id.'" selected="selected">'.$s->name.'</option>';
			else
				echo '<option value="'.$s->staff_id.'">'.$s->name.'</option>';
				
			echo "\n";
		}
		?>
    </select>
</div>
</div>

<div class="form-actions">
<?=form_button(array('name' => 'btnSubmit', 'class' => 'btn btn-primary btn-large', 'type' => 'submit', 'content' => 'Edit Module'))?>
</div>

<?=form_fieldset_close()?>

<?=form_close()?>