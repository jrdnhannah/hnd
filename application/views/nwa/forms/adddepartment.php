<?=form_open(current_url())?>

<?=form_fieldset('', array('class' => 'form-horizontal'))?>

<?=validation_errors()?>

<div class="control-group">
<?=form_label('Department:', 'department', array('class' => 'control-label'))?>
<div class="controls">
<?=form_input(array('name' => 'department', 'id' => 'department', 'required' => 'required', 'placeholder' => 'Department Name...', 'value' => set_value('department')))?>
</div>
</div>

<div class="form-actions">
<?=form_button(array('name' => 'btnSubmit', 'class' => 'btn btn-primary btn-large', 'type' => 'submit', 'content' => 'Add Department'))?>
</div>

<?=form_fieldset_close()?>

<?=form_close()?>