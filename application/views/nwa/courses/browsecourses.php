<div class="row">
<div class="span6 alert alert-error">
	<strong>Warning!</strong>
    <p>Deleting a course will erase the course, modules, assignments, and students' assignment submissions. This will be <strong>unrecoverable</strong>. Please make sure that you are confident in your decision before you delete a module.</p>
</div>

<?=$table?>
</div>