<div class="row">
<div class="span6 alert alert-error">
	<strong>Warning!</strong>
    <p>Before deleting a staff member, please ensure you have moved all of their modules to another staff member. You will not be able to remove them until you do so.</p>
</div>

<?=$table?>
</div>