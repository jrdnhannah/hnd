<?php

if($current_revision == false)
{
	$revision = '<em>No current revision.</em>';
}
else
{
	$file = explode('/', $current_revision);
	$file = $file[count($file) - 1];
	
	$revision = anchor(site_url().'uploads/students/'.$file, 'Click to Download');
}
?>

<p>Current Revision: <?=$revision?></p>