<div id="submitassignment">
<?php /* If there are any validation errors, echo them here */ ?>
<?php if(validation_errors()) echo '<div class="alert alert-error">'.validation_errors().'</div>'; ?>

<?=form_open_multipart(current_url(), array('class' => 'form-inline'))?>

<div class="control-group">
<?=form_label('Upload Assignment:', 'assignment', array('class' => 'control-label'))?>
<div class="controls">
<?php if($disable_form): ?>
<input type="file" name="assignment" id="assignment" class="disabled" disabled="disabled" required />
<?php else: ?>
<input type="file" name="assignment" id="assignment" required />
<?php endif; ?>
</div>
</div>

<div class="control-group">
<?php if($disable_form): ?>
<?=form_label(form_checkbox(array('name' => 'disclaimer', 'id' => 'disclaimer', 'value' => 'true', 'class' => 'checkbox disabled', 'required' => 'required', 'disabled' => 'disabled')).'I declare that this assignment is all my own work and that I will acknowledge all materials used from the published or unpublished works of other people.', 'disclaimer', array('class' => 'control-label checkbox'))?>
<?php else: ?>
<?=form_label(form_checkbox(array('name' => 'disclaimer', 'id' => 'disclaimer', 'value' => 'true', 'class' => 'checkbox', 'required' => 'required')).'I declare that this assignment is all my own work and that I will acknowledge all materials used from the published or unpublished works of other people.', 'disclaimer', array('class' => 'control-label checkbox'))?>
<?php endif; ?>
</div>

<div class="control-group">
<?php if($disable_form): ?>
<?=form_label(form_checkbox(array('name' => 'final', 'id' => 'final', 'value' => 'true', 'class' => 'checkbox disabled', 'disabled' => 'disabled')).'Mark as final?', 'final', array('class' => 'control-label checkbox'))?>
<?php else: ?>
<?=form_label(form_checkbox(array('name' => 'final', 'id' => 'final', 'value' => 'true', 'class' => 'checkbox')).'Mark as final?', 'final', array('class' => 'control-label checkbox'))?>
<?php endif; ?>
</div>


<?=form_hidden('assid', $assid)?>
<?php if($disable_form): ?>
<?=form_button(array('name' => 'btnSubmit', 'class' => 'btn btn-primary btn-medium disabled', 'type' => 'submit', 'content' => 'Upload', 'disabled' => 'disabled'))?>
<?php else: ?>
<?=form_button(array('name' => 'btnSubmit', 'class' => 'btn btn-primary btn-medium', 'type' => 'submit', 'content' => 'Upload'))?>
<?php endif; ?>

<?=form_close()?>

</div>