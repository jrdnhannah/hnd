<div class="form center">

<?php /* If there are any validation errors, echo them here */ ?>
<?php if(validation_errors()) echo '<div class="alert alert-error">'.validation_errors().'</div>'; ?>

<?=form_open('Home')?>
<?=form_fieldset('Please Log In to the SMU Assignment System', array('class' => 'form-horizontal'))?>

<div class="control-group">
<?=form_label('P Number:', 'pnumber', array('class' => 'control-label'))?>
<div class="controls">
<?=form_input(array('name' => 'pnumber', 'id' => 'pnumber', 'required' => 'required', 'placeholder' => 'P Number...', 'value' => set_value('pnumber')))?>
</div>
</div>

<div class="control-group">
<?=form_label('Password:', 'password', array('class' => 'control-label'))?>
<div class="controls">
<?=form_password(array('name' => 'password', 'id' => 'password', 'required' => 'required', 'placeholder' => 'Password...'))?>
</div>
</div>

<div class="control-group">
<?=form_label('Login Type:', 'type', array('class' => 'control-label'))?>
<div class="controls">
<?=form_dropdown('type', array('' => '', 'student' => 'Student', 'staff' => 'Staff', 'nwadmin' => 'Network Administrator'), 'student', 'id="type" required')?>
</div>
</div>

<div class="form-actions">
<?=form_button(array('name' => 'btnSubmit', 'class' => 'btn btn-primary btn-large', 'type' => 'submit', 'content' => 'Log In'))?>
</div>

<?=form_fieldset_close()?>
<?=form_close()?>

</div>