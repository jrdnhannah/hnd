<!DOCTYPE html>

<html>
<head>
<title><?=$title?></title>
<base href="http://hnd.wp7solutions.co.uk" />
<?php /* include CSS */ ?>
<link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap-responsive.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/my.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/dateinput.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/jquery-ui-1.8.18.custom.css" />
<?php /* include JS and jQuery */ ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>
<script type="text/javascript" src="js/bootstrap-dropdown.js"></script>
<script src="http://cdn.jquerytools.org/1.2.6/form/jquery.tools.min.js"></script>
<script type="text/javascript" src="js/js.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>
</head>
<body>

<div class="container">