<?php /* If there are any validation errors, echo them here */ ?>
<?php if(validation_errors()) echo '<div class="alert alert-error">'.validation_errors().'</div>'; ?>

<?=form_open_multipart(current_url())?>
<?=form_fieldset('Create an Assigment', array('class' => 'form-horizontal'))?>

<div class="control-group">
<?=form_label('Assignment Title:', 'title', array('class' => 'control-label'))?>
<div class="controls">
<input type="text" name="title" id="title" placeholder="Title..." value="<?=set_value('title')?>" required />
</div>
</div>

<div class="control-group">
<?=form_label('Due Date:', 'date', array('class' => 'control-label'))?>
<div class="controls">
<input type="date" name="date" id="date" value="<?=set_value('date', date('Y-m-d'))?>" required />
</div>
</div>

<div class="control-group">
<?=form_label('Percentage', 'percentage', array('class' => 'control-label'))?>
<div class="controls">
<div class="input-append">
<input type="number" name="percentage" id="percentage" placeholder="Percentage..." value="<?=set_value('percentage', 0)?>" class="input-small" required />
<span class="add-on">%</span>
</div>
</div>
</div>

<div class="control-group">
<?=form_label('Assignment Brief:', 'brief', array('class' => 'control-label'))?>
<div class="controls">
<input type="file" name="brief" id="brief" required />
</div>
</div>

<?php /* create valid submission types */ ?>
<?php

$options = array (
				'doc,docx,odt,rtf' => 'Document',
				'ppt,pptx' => 'Presentation',
				'xls,xlsx' => 'Spreadsheet',
				'zip,rar,gzip' => 'ZIP/RAR Archive'
			);

?>

<div class="control-group">
<?=form_label('Valid Submission Types:', 'types', array('class' => 'control-label'))?>
<div class="controls">
<?=form_multiselect('types[]', $options, '', 'id="types" required')?>
</div>
</div>

<div class="form-actions">
<?=form_button(array('name' => 'btnSubmit', 'class' => 'btn btn-primary btn-large', 'type' => 'submit', 'content' => 'Create Assignment'))?>
</div>

<?=form_fieldset_close()?>

<?=form_close()?>