<h3>Mark Assignment</h3>

<div data-role="dialog" style="margin:2px 10px 0 5px;">
<p>Assignment Title: <?=$assignment?><br />
Student: <?=$student?><br /></p>

<?=form_open(current_url(), array('data-role' => 'markassignment'))?>

<div class="control-group">
<?=form_label('Percentage', 'percentage', array('class' => 'control-label'))?>
<div class="controls">
<div class="input-append">
<input type="number" min="0" max="100" name="percentage" id="percentage" placeholder="Percentage..." value="<?=set_value('percentage', 0)?>" class="input-small" required />
<span class="add-on">%</span>
</div>
</div>
</div>


<?php
// explode $grades into an array
$grades = explode(',', $grades);
// loop through and put the $grades array into a $key => $value array
$options = array('' => '');

foreach($grades as $value)
{
	$options[strtolower($value)] = $value;
}

?>

<div class="control-group">
<?=form_label('Grade', 'grades', array('class' => 'control-label'))?>
<div class="controls">
<?=form_dropdown('grades', $options, '', 'id="grades" required')?>
</div>
</div>

<div class="control-group">
<?=form_label('Feedback', 'feedback', array('class' => 'control-label'))?>
<div class="controls">
<textarea name="feedback" id="feedback" placeholder="Feedback..." cols="40" rows="15" required><?=set_value('feedback')?></textarea>
</div>
</div>

<div class="form-actions">
<?=form_button(array('name' => 'btnSubmit', 'class' => 'btn btn-primary btn-medium', 'type' => 'submit', 'content' => 'Submit Feedback'))?>
</div>

<?=form_close()?>

</div>