<?php if($error == 'ext'): ?>

<h2>Invalid Extension</h2>

<p>The file you uploaded was not in the correct file format. Please go back, check the file extensions allowed and upload an appropriate file.<br />
The file has been removed from the system and your assignment brief has <em>not</em> been uploaded.</p>

<?php elseif ($error == 'virus'): ?>

<h2>Virus Detected</h2>
<p>There was a virus detected in the file you uploaded. Please make sure any files you upload are clean from viruses.<br />
The file has been removed from the system and your assignment brief has <em>not</em> been uploaded.</p>

<?php endif; ?>