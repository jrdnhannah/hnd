<?php if( ! defined('BASEPATH') ) exit('No direct script access!');

class UserDB extends CI_Model
{
	#########################################
	#	Check if username is valid			#
	#	public								#
	#########################################
	
	public function is_valid_id($id, $type = 'student')
	{
		switch(strtolower($type))
		{
			case 'student':
				$this->db->select('pnumber')
						 ->from('tblStudents')
						 ->where('pnumber', $id);
			break;
			case 'staff':	
				$this->db->select('staff_id')
						 ->from('tblStaff')
						 ->where('staff_id', $id);
			break;
		}
		
		$query = $this->db->get();
		
		if($query->num_rows() == 0)
			return false;
		else
			return true;
	}
	
	#########################################
	#	Check if username + pass are valid	#
	#	public								#
	#########################################
	
	public function is_valid_account($id, $pass, $type = 'student')
	{
		switch(strtolower($type))
		{
			case 'student':
				$this->db->select('pnumber')
					 	 ->from('tblStudents')
						 ->where('pnumber', $id)
						 ->where('password', $pass);
			break;
			case 'staff':
				$this->db->select('staff_id')
						 ->from('tblStaff')
						 ->where('staff_id', $id)
						 ->where('password', $pass);
			break;
		}
		
		$query = $this->db->get();
		
		if($query->num_rows() == 0)
			return false;
		else
			return true;
	}
}


/* End of File */
/* Location: ./application/models/user.php */