<?php if( ! defined('BASEPATH') ) exit('No direct script access!');

class Courses extends CI_Model
{
	public function __construct()
	{
		parent::__construct();	
	}
	
	#########################################
	#	Get course							#
	#	public								#
	#########################################
	
	public function get_course($id)
	{
		$this->db->select()
				 ->from('tblCourses')
				 ->where('ID', $id)
				 ->limit(1);
		
		$query = $this->db->get();
		
		return $query->result();
	}
	
	#########################################
	#	Get course list						#
	#	public								#
	#########################################
	
	public function get_course_list()
	{
		$this->db->select('`c`.`ID`, CONCAT(`ct`.`type`, " ", `c`.`name`) AS `Name`', FALSE)
				 ->from('tblCourses `c`, tblCourseType `ct`')
				 ->where('`c`.`type` = `ct`.`ID`')
				 ->order_by('`ID`');
		
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	#########################################
	#	Get course list	with department		#
	#	name								#
	#	public								#
	#########################################
	
	public function get_course_list_with_department_names()
	{
		$this->db->select('`c`.`ID`, CONCAT(`ct`.`type`, " ", `c`.`name`) AS `coursename`, `d`.`name` AS `department`', FALSE)
				 ->from('tblCourses `c`, tblDepartments `d`, tblCourseType `ct`')
				 ->where('`c`.`type` = `ct`.`ID`')
				 ->where('`c`.`dept` = `d`.`ID`');
		
		$query = $this->db->get();
		
		return $query->result();
	}
	
	#########################################
	#	Add Course							#
	#	public								#
	#########################################
	
	public function add_course($type, $name, $dept, $description = NULL)
	{
		$data = array(
					'type' => $type,
					'name' => $name,
					'dept' => $dept
						);
		
		// is there a description?
		if($description != NULL) $data['description'] = $description;
		
		if($this->db->insert('tblCourses', $data))
			return true;
		else
			return false;
	}
	
	#########################################
	#	Update Course						#
	#	public								#
	#########################################
	
	public function update_course($id, $type, $name, $description, $department)
	{
		// start transaction		
		$data = array(
					'type' => $type,
					'name' => $name,
					'description' => $description,
					'dept' => $department
					);
		$this->db->where('ID', $id);

		if($this->db->update('tblCourses', $data))
			return true;
		else
			return false;
	}
	
	#########################################
	#	Delete Course						#
	#	public								#
	#########################################
	
	public function delete_course($id, $trans = 1)
	{
		// using transactions? (when this method is called in other methods using transactions, transactions must NOT be used
		if($trans === 1)
			$this->db->trans_begin();
		
		// got to delete all modules within a course, so might as well use the modules model
		$ci =& get_instance();
		$ci->load->model('modules');
		
		// get all the modules from tblCourseModules
		$this->db->select('module_id')
				 ->from('tblCourseModules')
				 ->where('course_id', $id);
		
		$query = $this->db->get();
		
		// delete the module
		foreach($query->result() as $m)
		{
			$this->modules->delete_module($m->module_id, 0);
		}
		
		// delete course
		$this->db->where('ID',$id)
				 ->delete('tblCourses');
		
		if($trans === 1)
		{
			if($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
				return false;
			}
			else
			{
				$this->db->trans_commit();
				return true;
			}
		}
		else
		{
			return true;	
		}
	}
}

/* End of File */
/* File Location: ./application/models/Courses.php */