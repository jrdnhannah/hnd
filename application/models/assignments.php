<?php if( ! defined('BASEPATH') ) exit('No direct script access!');

class Assignments extends CI_Model
{
	#########################################
	#	Get assignment list for module		#
	#	public								#
	#########################################
	
	public function get_assignments($cid, $mid)
	{
		$this->db->select('`a`.`ID`, `a`.`name`, `a`.`duedate`', FALSE)
				 ->from('tblAssignments `a`, tblCourseModules `cm`')
				 ->where('`a`.`module_id` = `cm`.`module_id`', NULL, FALSE)
				 ->where('`cm`.`module_id`', $mid, FALSE)
				 ->order_by('`a`.`duedate`');
				 
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	#########################################
	#	Get assignment list for staff		#
	#	public								#
	#########################################
	
	public function get_staff_assignments($mid, $cid, $sid)
	{
		$this->db->select('`a`.`ID` AS `assignmentID`, `a`.`name`, `a`.`duedate`, `m`.`ID` AS `moduleID`, `c`.`ID` AS `courseID`', FALSE)
				 ->from('tblAssignments `a`, tblCourseModules `cm`, tblModules `m`, `tblCourses` `c`')
				 ->where('`m`.`ID`', $mid, FALSE)
				 ->where('`c`.`ID`', $cid, FALSE)
				 ->where('`cm`.`course_id` = `c`.`ID`', NULL, FALSE)
				 ->where('`cm`.`module_id` = `m`.`ID`', NULL, FALSE)
				 ->where('`a`.`module_id` = `m`.`ID`', NULL, FALSE)
				 ->where('`m`.`taught_by`', $sid)
				 ->order_by('`a`.`duedate`');
				 
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	#########################################
	#	Get assignment brief				#
	#	public								#
	#########################################
	
	public function get_assignment_brief($aid)
	{
		$this->db->select('`a`.`name`, CONCAT(`s`.`firstName`, " ", `s`.`lastName`) AS `lecturer`, `a`.`duedate`, `a`.`specification_file`, `a`.`formats`', FALSE)
				 ->from('`tblAssignments` `a`, `tblStaff` `s`, `tblModules` `m`')
				 ->where('`a`.`module_id` = `m`.`ID`', NULL, FALSE)
				 ->where('`m`.`taught_by` = `s`.`staff_id`', NULL, FALSE)
				 ->where('`a`.`ID`', $aid, FALSE);
		$query = $this->db->get();
		$result = $query->result_array();
		
		// the query returns an array of arrays, we only need the first index of the outer array
		return $result[0];
	}
	
	#########################################
	#	Get valid file types for assignment	#
	#	public								#
	#########################################
	
	public function get_valid_file_types($aid)
	{
		$this->db->select('formats')
				 ->from('tblAssignments')
				 ->where('ID', $aid);
		
		$query = $this->db->get();
		$result = $query->row();
		return $result->formats;
	}
	
	#########################################
	#	Is assignment ID valid?				#
	#	public								#
	#########################################
	
	public function is_valid_id($aid)
	{
		$this->db->select('ID')
				 ->from('tblAssignments')
				 ->where('ID', $aid);
		$query = $this->db->get();
		
		if($query->num_rows() == 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	#########################################
	#	Is the current assignment revision	#
	#	final?								#
	#	public								#
	#########################################
	
	public function is_current_revision_final(Student $student, $aid)
	{
		$this->db->select('is_final')
				 ->from('tblAssignmentSubmissions')
				 ->where('student_id', $student->data['id'])
				 ->where('assignment_id', $aid);
		
		$query	= $this->db->get();
		
		// if no rows are returned, then no assignment has been uploaded yet
		if($query->num_rows() == 0) return false;
		
		// should only return one row
		$row	= $query->row();
				
		if($row->is_final == 1)
			return true;
		else
			return false;
	}
	
	#########################################
	#	Insert uploaded submission into db	#
	#	public								#
	#########################################
	
	public function submit_assignment(Student $student, $location)
	{
		$sid	= $student->data['id'];
		$aid	= $this->input->post('assid');
		$final	= $this->input->post('final') ? 1 : 0;
		$date	= date('Y-m-d H:i:s');
		
		// check if student has already uploaded an assignment
		$this->db->select('ID, location')
				 ->from('tblAssignmentSubmissions')
				 ->where('student_id', $sid)
				 ->where('assignment_id', $aid);
		
		$query	= $this->db->get();
		
		// if they have, edit the assignment that already exists, also delete the exiting file
		if($query->num_rows() == 1)
		{
			// should only ever return one row
			$row = $query->row();
			// remove
			exec('rm ' . $row->location, $out, $int);
			
			$update_data	= array (
								'is_final' => $final,
								'location' => $location,
								'uploadDate' => $date
							  );
			$this->db->where('assignment_id', $aid);
			
			// update
			if($this->db->update('tblAssignmentSubmissions', $update_data))
				return true;
			else
				return false;
		}
		// otherwise, insert new row
		else
		{
			$insert_data	= array (
								'student_id' => $sid,
								'assignment_id' => $aid,
								'is_final' => $final,
								'location' => $location,
								'uploadDate' => $date
							  );
			if($this->db->insert('tblAssignmentSubmissions', $insert_data))
				return true;
			else
				return false;
		}
	}
	
	#########################################
	#	Get current revision of assignment	#
	#	public								#
	#########################################
	
	public function get_current_revision(Student $student, $aid)
	{
		$this->db->select('location')
				 ->from('tblAssignmentSubmissions')
				 ->where('student_id', $student->data['id'])
				 ->where('assignment_id', $aid);

		$query	= $this->db->get();
		
		// if no current revision
		if($query->num_rows() == 0)
		{
			return false;
		}
		// current revision listed
		else
		{
			$row = $query->row();
			
			return $row->location;
		}
	}
	
	#########################################
	#	Insert uploaded brief into db		#
	#	public								#
	#########################################
	
	public function add_assignment($file, $mid)
	{
		$name		= $this->input->post('title');
		$percentage	= $this->input->post('percentage');
		$date		= date('Y-m-d');
			
		$formats	= '';
				
		foreach($this->input->post('types') as $value)
		{
			$formats .= $value . ',';
		}
		
		// remove last comma from format list
		$formats = substr($formats, 0, -1);
		
		$insert_data	= array (
							'name' => $name,
							'module_id' => $mid,
							'duedate' => $date,
							'percentage' => $percentage,
							'specification_file' => $file,
							'formats' => $formats
						  );

		if($this->db->insert('tblAssignments', $insert_data))
			return true;
		else
			return false;
	}
	
	#########################################
	#	Get current submissions for			#
	#	current course/module				#
	#	public								#
	#########################################
	
	public function get_submitted_assignments($cid, $mid, $aid)
	{
		$this->db->select('CONCAT(`s`.`firstName`, " ", `s`.`lastName`) AS `name`, `s`.`pnumber`, `as`.`location`, `as`.`ID`', FALSE)
				 ->from('`tblStudents` `s`, `tblAssignmentSubmissions` `as`, `tblAssignments` `a`, `tblCourses` `c`, `tblCourseModules` `cm`, `tblModules` `m`', FALSE)
				 ->where('`as`.`student_id` = `s`.`pnumber`', NULL, FALSE)
				 ->where('`as`.`assignment_id` = `a`.`ID`', NULL, FALSE)
				 ->where('`a`.`module_id` = `m`.`ID`', NULL, FALSE)
				 ->where('`a`.`module_id` = `cm`.`module_id`', NULL, FALSE)
				 ->where('`c`.`ID` = `cm`.`course_id`', NULL, FALSE)
				 ->where('`c`.`ID`', $cid, FALSE)
				 ->where('`m`.`ID`', $mid, FALSE)
				 ->where('`a`.`ID`', $aid, FALSE);
		
		$query = $this->db->get();
		return $query->result_array();
	}
	
	#########################################
	#	Get Assignment Name					#
	#	Public								#
	#########################################
	
	public function get_assignment_name($aid)
	{
		$this->db->select('name')
				 ->from('tblAssignments')
				 ->where('ID', $aid);
		
		$query = $this->db->get();
		
		if($query->num_rows() == 0)
		{
			return false;
		}
		else
		{
			$row = $query->row();
			return $row->name;
		}
	}
	
	#########################################
	#	Get Assignment Due Date				#
	#	Public								#
	#########################################
	
	public function get_assignment_duedate($aid)
	{
		$this->db->select('duedate')
				 ->from('tblAssignments')
				 ->where('ID', $aid);
		
		$query = $this->db->get();
		
		if($query->num_rows() == 0)
		{
			return false;
		}
		else
		{
			$row = $query->row();
			return $row->duedate;
		}
	}
	
	#########################################
	#	Get Assignment Submission ID		#
	#	Public								#
	#########################################
	
	public function get_assignment_submission_id($sid, $aid)
	{
		$this->db->select('ID')
				 ->from('tblAssignmentSubmissions')
				 ->where('student_id',$sid)
				 ->where('assignment_id',$aid)
				 ->limit(1);
		$query = $this->db->get();
		
		$result = $query->row();
		
		return $result->ID;
	}
	
	#########################################
	#	Mark Assignment						#
	#	Public								#
	#########################################
	
	public function mark_assignment($sid, $aid, $said, $grade, $percentage, $feedback)
	{
		$data = array(
					'student_id' => $sid,
					'assignment_id' => $aid,
					'student_assignment_id' => $said,
					'grade' => $grade,
					'percentage' => $percentage,
					'feedback' => $feedback
					);	
					
		if($this->db->insert('tblAssignmentMarks', $data))
			return true;
		else
			return false;
	}
	
	#########################################
	#	Is assignment marked?				#
	#	Public								#
	#########################################
	
	public function is_marked($sid, $aid)
	{
		$this->db->select()
				 ->from('tblAssignmentMarks')
				 ->where('student_id', $sid)
				 ->where('assignment_id', $aid);
		
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
			return true;
		else
			return false;
	}
}

/* End of File */
/* Location: ./application/models/Assignments.php */