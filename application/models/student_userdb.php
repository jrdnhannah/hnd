<?php if( ! defined('BASEPATH') ) exit('No direct script access!');

require_once('IUserDB.php');

class Student_UserDB extends CI_Model implements IUserDB
{
	
	#########################################
	#	Check if username is valid			#
	#	public								#
	#########################################
	
	public function is_valid_id($id)
	{		
		$this->db->select('pnumber')
				 ->from('tblStudents')
				 ->where('pnumber', $id);
						 
		$query = $this->db->get();
		
		if($query->num_rows() == 0)
			return false;
		else
			return true;
	}
	
	#########################################
	#	Check if username + pass are valid	#
	#	public								#
	#########################################
	
	public function is_valid_account($id, $pass)
	{
		$this->db->select('pnumber')
				 ->from('tblStudents')
				 ->where('pnumber', $id)
				 ->where('password', $pass);

		$query = $this->db->get();
		
		if($query->num_rows() == 0)
			return false;
		else
			return true;
	}
	
	#########################################
	#	Get full name						#
	#	public								#
	#########################################
	
	public function get_full_name($user)
	{
		$this->db->select('CONCAT(`firstName`, " ", `lastName`) AS `name`', false)
				 ->from('tblStudents')
				 ->where('pnumber', $user);
		$query = $this->db->get();
		$query = $query->row();
		
		return $query->name;
	}
	
	#########################################
	#	Get first name						#
	#	public								#
	#########################################
	
	public function get_first_name($user)
	{
		$this->db->select('firstName')
				 ->from('tblStudents')
				 ->where('pnumber', $user);
		$query = $this->db->get();
		$query = $query->row();
		
		return $query->firstName;
	}
	
	#########################################
	#	Get last name						#
	#	public								#
	#########################################
	
	public function get_last_name($user)
	{
		$this->db->select('lastName')
				 ->from('tblStudents')
				 ->where('pnumber', $user);
		$query = $this->db->get();
		$query = $query->row();
		
		return $query->lastName;
	}
	
	#########################################
	#	Get course ID						#
	#	public								#
	#########################################
	
	public function get_course_id($user)
	{
		$this->db->select('course_id')
				 ->from('tblStudents')
				 ->where('pnumber', $user);
		$query = $this->db->get();
		$query = $query->row();
		
		return $query->course_id;
	}
	
	#########################################
	#	Get course name						#
	#	public								#
	#########################################
	
	public function get_course_name($cid)
	{
		$this->db->select('name')
				 ->from('tblCourses')
				 ->where('ID', $cid);
		$query = $this->db->get();
		$query = $query->row();
		
		return $query->name;
	}
	
	#########################################
	#	Get year							#
	#	public								#
	#########################################
	
	public function get_year($id)
	{
		$this->db->select('year')
				 ->from('tblStudents')
				 ->where('pnumber', $id);
		$query = $this->db->get();
		$query = $query->row();
		
		return $query->year;
	}
	
	#########################################
	#	Get marks							#
	#	public								#
	#########################################
	
	public function get_marks($id)
	{							
		$this->db->select('`a`.`name` AS `AssignmentName`, `m`.`name` AS `ModuleName`, `am`.`grade`, `am`.`feedback`', false)
				 ->from('tblAssignments `a`, tblModules `m`, tblCourseModules `cm`, tblStudents `s`, tblAssignmentMarks `am`')
				 ->where('`a`.`module_id` = `m`.`ID`')
				 ->where('`cm`.`module_id` = `m`.`ID`')
				 ->where('`cm`.`course_id` = `s`.`course_id`')
				 ->where('`s`.`pnumber`',$id)
				 ->where('`am`.`assignment_id` = `a`.`ID`');
							
		$query = $this->db->get();
		$marks = $query->result();
		
		return $marks;
	}
	
	#########################################
	#	Get list of students				#
	#	public								#
	#########################################
	
	public function get_students($offset = 0)
	{
		// if offset is not a number, this will make it into 0
		$offset = (int) $offset;
		
		$this->db->select()
				 ->from('tblStudents')
				 ->order_by('pnumber')
				 ->limit(30,$offset);
		
		$query = $this->db->get();
		return $query->result();
	}
	
	#########################################
	#	Get a student						#
	#	public								#
	#########################################
	
	public function get_student($id)
	{
		$this->db->select()
				 ->from('tblStudents')
				 ->where('pnumber', $id)
				 ->limit(1);
		$query = $this->db->get();
		
		return $query->row();
	}
	
	#########################################
	#	Add student							#
	#	public								#
	#########################################
	
	public function add_student($id, $firstname, $lastname, $password, $gender, $address, $email, $course_id, $startdate, $enddate, $year)
	{
		$data	= array(
					'pnumber' => $id,
					'firstName' => $firstname,
					'lastName' => $lastname,
					'gender' => $gender,
					'address' => $address,
					'password' => md5($password),
					'email' => $email,
					'course_id' => $course_id,
					'startdate' => $startdate,
					'enddate' => $enddate,
					'year' => $year
					);
				
	if($this->db->insert('tblStudents', $data))
			return true;
		else
			return false;
	}
	
	#########################################
	#	Update student						#
	#	public								#
	#########################################
	
	public function update_student($id, $firstname, $lastname, $password = NULL, $gender, $address, $email, $course_id, $startdate, $enddate)
	{
		$data	= array(
					'firstName' => $firstname,
					'lastName' => $lastname,
					'gender' => $gender,
					'address' => $address,
					'email' => $email,
					'course_id' => $course_id,
					'startdate' => $startdate,
					'enddate' => $enddate
					);
		
		if($password != NULL)
			$data['password'] = md5($password);
				
		$this->db->where('pnumber', $id);
					
		if($this->db->update('tblStudents', $data))
			return true;
		else
			return false;
	}
	
	#########################################
	#	Delete student						#
	#	public								#
	#########################################
	
	public function delete_student($id)
	{
		// done in this order to preserve foreign keys etc
		
		// delete assignment submission files
		$this->db->select('location')
				 ->from('tblAssignmentSubmissions')
				 ->where('student_id', $id);

		$query = $this->db->get();
		$files = $query->result_array();
		
		foreach($files as $f)
		{
			shell_exec('rm ' . $f['location']);
		}
			
		
		// delete assignment submission DB entries
		
		$this->db->where('student_id',$id)
				 ->delete('tblAssignmentSubmissions');
				 
		// delete student
		$this->db->where('pnumber',$id)
				 ->delete('tblStudents');
		
		return true;
	}
}


/* End of File */
/* Location: ./application/models/Student_UserDB.php */