<?php if( ! defined('BASEPATH') ) exit ('No direct script access!');

require_once('IUserDB.php');

class nwa_userdb extends CI_Model implements IUserDB
{
	#########################################
	#	Check if username is valid			#
	#	public								#
	#########################################
	
	public function is_valid_id($id)
	{		
		$this->db->select('ID')
				 ->from('tblNetworkAdmins')
				 ->where('ID', $id);
						 
		$query = $this->db->get();
		
		if($query->num_rows() == 0)
			return false;
		else
			return true;
	}
	
	#########################################
	#	Check if username + pass are valid	#
	#	public								#
	#########################################
	
	public function is_valid_account($id, $pass)
	{
		$this->db->select('ID')
				 ->from('tblNetworkAdmins')
				 ->where('ID', $id)
				 ->where('Password', $pass);

		$query = $this->db->get();
		
		if($query->num_rows() == 0)
			return false;
		else
			return true;
	}
	
	#########################################
	#	Get full name						#
	#	public								#
	#########################################
	
	public function get_full_name($user)
	{
		$this->db->select('CONCAT(`firstName`, " ", `lastName`) AS `name`', false)
				 ->from('tblNetworkAdmins')
				 ->where('ID', $user);
		$query = $this->db->get();
		$query = $query->row();
		
		return $query->name;
	}
	
	#########################################
	#	Get first name						#
	#	public								#
	#########################################
	
	public function get_first_name($user)
	{
		$this->db->select('firstName')
				 ->from('tblNetworkAdmins')
				 ->where('ID', $user);
		$query = $this->db->get();
		$query = $query->row();
		
		return $query->firstName;
	}
	
	#########################################
	#	Get last name						#
	#	public								#
	#########################################
	
	public function get_last_name($user)
	{
		$this->db->select('lastName')
				 ->from('tblNetworkAdmins')
				 ->where('ID', $user);
		$query = $this->db->get();
		$query = $query->row();
		
		return $query->lastName;
	}
}

/* End of File */
/* File Location: ./application/models/nwa_userdb.php */