<?php if ( ! defined('BASEPATH') ) exit('No direct script access!');

require_once('IUserDB.php');

class Staff_UserDB extends CI_Model implements IUserDB
{
	
	#########################################
	#	Check if username is valid			#
	#	public								#
	#########################################

	public function is_valid_id($id)
	{
		$this->db->select('staff_id')
				 ->from('tblStaff')
				 ->where('staff_id', $id);
						 
		$query = $this->db->get();
		
		if($query->num_rows() == 0)
			return false;
		else
			return true;
	}
	
	#########################################
	#	Check if username + pass are valid	#
	#	public								#
	#########################################
	
	public function is_valid_account($id, $pass)
	{
		$this->db->select('staff_id')
				 ->from('tblStaff')
				 ->where('staff_id', $id)
				 ->where('password', $pass);

		$query = $this->db->get();
		
		if($query->num_rows() == 0)
			return false;
		else
			return true;
	}
	
	#########################################
	#	Get full name						#
	#	public								#
	#########################################
	
	public function get_full_name($user)
	{
		$this->db->select('CONCAT(`firstName`, " ", `lastName`) AS `name`', false)
				 ->from('tblStaff')
				 ->where('staff_id', $user);
		$query = $this->db->get();
		$query = $query->row();
		
		return $query->name;
	}
	
	#########################################
	#	Get first name						#
	#	public								#
	#########################################
	
	public function get_first_name($user)
	{
		$this->db->select('firstName')
				 ->from('tblStaff')
				 ->where('staff_id', $user);
		$query = $this->db->get();
		$query = $query->row();
		
		return $query->firstName;
	}
	
	#########################################
	#	Get last name						#
	#	public								#
	#########################################
	
	public function get_last_name($user)
	{
		$this->db->select('lastName')
				 ->from('tblStaff')
				 ->where('staff_id', $user);
		$query = $this->db->get();
		$query = $query->row();
		
		return $query->lastName;
	}
	
	#########################################
	#	Get dept ID							#
	#	public								#
	#########################################
	
	public function get_dept($user)
	{
		$this->db->select('dept')
				 ->from('tblStaff')
				 ->where('staff_id', $user);
		$query = $this->db->get();
		$query = $query->row();
		
		return $query->dept;
	}
	
	#########################################
	#	Get dept name						#
	#	public								#
	#########################################
	
	public function get_dept_name($did)
	{
		$this->db->select('name')
				 ->from('tblDepartments')
				 ->where('ID', $did);
		$query = $this->db->get();
		$query = $query->row();
		
		return $query->name;
	}
	
	#########################################
	#	Get student course type				#
	#	public								#
	#########################################
	
	public function get_course_type($id)
	{
		$this->db->select('`c`.`type`', false)
				 ->from('`tblCourses` `c`, `tblStudents` `s`', false)
				 ->where('`s`.`pnumber`', "'".$id."'", false)
				 ->where('`s`.`course_id` = `c`.`ID`');
		$query = $this->db->get();
		$query = $query->row();
		
		return $query->type;
	}
	
	#########################################
	#	Get grades for course type			#
	#	public								#
	#########################################
	
	public function get_course_marks($ctype)
	{
		$this->db->select('marks')
				 ->from('tblCourseType')
				 ->where('ID', $ctype);
				 
		$query = $this->db->get();
		$query = $query->row();
		
		return $query->marks;
	}
	
	#########################################
	#	Get lecturers						#
	#	public								#
	#########################################
	
	public function get_lecturers()
	{
		$this->db->select('`staff_id`, CONCAT(`firstName`, " ", `lastName`) AS `name`', FALSE)
				 ->from('tblStaff');
				 
		$query = $this->db->get();
		
		return $query->result();
	}
	
	#########################################
	#	Get lecturers all info				#
	#	public								#
	#########################################
	
	public function get_lecturers_all_info()
	{
		$this->db->select()
			 ->from('tblStaff');
		
		$query = $this->db->get();
		
		return $query->result();	
	}
	
	#########################################
	#	Get all info on a lecturer			#
	#	public								#
	#########################################
	
	public function get_lecturer($id)
	{
		$this->db->select()
				 ->from('tblStaff')
				 ->where('staff_id',$id);
		$query = $this->db->get();
		
		$l = $query->result();
		
		return $l[0];	
	}
	
	#########################################
	#	Add Staff							#
	#	public								#
	#########################################
	
	public function add_staff($id, $fname, $lname, $pass, $addr, $email, $num, $dept)
	{
		$data = array(
					'staff_id' => $id,
					'firstName' => $fname,
					'lastName' => $lname,
					'password' => md5($pass),
					'address' => $addr,
					'email' => $email,
					'contactNumber' => $num,
					'dept' => $dept
					);
				
		if($this->db->insert('tblStaff', $data))
			return true;
		else
			return false;
	}
	
	#########################################
	#	Edit Staff							#
	#	public								#
	#########################################
	
	public function edit_staff($id, $fname, $lname, $pass, $addr, $email, $num, $dept)
	{
		$data = array(
					'firstName' => $fname,
					'lastName' => $lname,
					'address' => $addr,
					'email' => $email,
					'contactNumber' => $num,
					'dept' => $dept
					);
		if($pass != NULL) $data['password'] = md5($pass);
		
		$this->db->where('staff_id',$id);
		
		if($this->db->update('tblStaff', $data))
			return true;
		else
			return false;
	}
	
	#########################################
	#	Delete Staff Member					#
	#	public								#
	#########################################
	
	public function delete_staff($id)
	{
		$this->db->where('staff_id',$id);
		
		if($this->db->delete('tblStaff'))
			return true;
		else
			return false;	
	}
}

/* End of File */
/* File Location: ./application/models/staff_userdb.php */