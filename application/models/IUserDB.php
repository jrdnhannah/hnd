<?php if( ! defined('BASEPATH') ) exit ('No direct script access!');

interface IUserDB
{
	public function is_valid_id($id);
	public function is_valid_account($id, $pass);
	public function get_full_name($id);
	public function get_first_name($id);
	public function get_last_name($id);
}

/* End of File */
/* File Location: ./application/models/IUserDB.php */