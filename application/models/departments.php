<?php if( ! defined('BASEPATH') ) exit('No direct script access!');

class Departments extends CI_Model
{
	public function __construct()
	{
		parent::__construct();	
	}
	
	#########################################
	#	Get departments						#
	#	public								#
	#########################################
	
	public function get_departments()
	{
		$this->db->select()
				 ->from('tblDepartments')
				 ->order_by('name');
		
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	#########################################
	#	Get department						#
	#	public								#
	#########################################
	
	public function get_department($id)
	{
		$this->db->select()
				 ->from('tblDepartments')
				 ->where('ID',$id);
		
		$query	= $this->db->get();
		$result = $query->result();

		return $result[0];
	}
	
	#########################################
	#	Add Departments						#
	#	public								#
	#########################################
	
	public function add_department($name)
	{
		$data = array('name' => $name);
		
		if($this->db->insert('tblDepartments', $data))
			return true;
		else
			return false;
	}
	
	#########################################
	#	Delete Department					#
	#	public								#
	#########################################
	
	public function delete_department($id, $trans = 1)
	{
		// using transactions? (when this method is called in other methods using transactions, transactions must NOT be used
		if($trans === 1)
			$this->db->trans_begin();
		
		// got to delete all modules within a course, so might as well use the modules model
		$ci =& get_instance();
		$ci->load->model('courses');
		
		// get all the courses from tblCourses
		$this->db->select('ID')
				 ->from('tblCourses')
				 ->where('dept', $id);
		
		$query = $this->db->get();
		
		// delete the module
		foreach($query->result() as $c)
		{
			$this->courses->delete_course($c->ID, 0);
		}
		
		// delete department
		$this->db->where('ID',$id)
				 ->delete('tblDepartments');
		
		if($trans === 1)
		{
			if($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
				return false;
			}
			else
			{
				$this->db->trans_commit();
				return true;
			}
		}
		else
		{
			return true;	
		}
	}
	
	#########################################
	#	Edit Department						#
	#	public								#
	#########################################
	
	public function edit_department($id, $name)
	{
		$data = array('name' => $name);
		
		$this->db->where('ID', $id);
		
		if($this->db->update('tblDepartments',$data))
			return true;
		else
			return false;
	}
}

/* End of File */
/* File Location: ./application/models/Departments.php */