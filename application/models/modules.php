<?php if( ! defined('BASEPATH') ) exit('No direct script access!');

class Modules extends CI_Model
{
	public function __construct()
	{
		parent::__construct();	
	}
	
	#########################################
	#	Get module list for student			#
	#	public								#
	#########################################
	
	public function get_module_list($sid)
	{
		$this->db->select('`m`.`name` AS `modName`, CONCAT(`sta`.`firstName`, " ", `sta`.`lastName`) AS `name`, `m`.`ID`', FALSE)
				 ->from('tblModules `m`, tblStaff `sta`, tblCourseModules `cm`')
				 ->where('`cm`.`course_id` = (SELECT `s`.`course_id` FROM `tblStudents` `s` WHERE `s`.`pnumber` = \'' . $sid . '\' AND `cm`.`year` = `s`.`year`)', NULL, FALSE)
				 ->where('`sta`.`staff_id` = `m`.`taught_by`', NULL, FALSE)
				 ->where('`m`.`ID` = `cm`.`module_id`', NULL, FALSE);
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	#########################################
	#	Get module list for staff			#
	#	public								#
	#########################################
	
	public function get_staff_modules($id)
	{
		$this->db->select('`m`.`name` AS `modName`, `m`.`ID` AS `moduleID`, `c`.`name` AS `courseName`, `cm`.`year`, `c`.`ID` AS `courseID`', FALSE)
				 ->from('tblModules `m`, tblStaff `sta`, tblCourseModules `cm`, tblCourses `c`')
				 ->where('`m`.`taught_by` = (SELECT `sta`.`staff_id` FROM `tblStaff` `sta` WHERE `sta`.`staff_id` = \'' . $id . '\')', NULL, FALSE)
				 ->where('`sta`.`staff_id` = `m`.`taught_by`', NULL, FALSE)
				 ->where('`m`.`ID` = `cm`.`module_id`', NULL, FALSE)
				 ->where('`c`.`ID` = `cm`.`course_id`', NULL, FALSE);
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	#########################################
	#	Get all modules						#
	#	public								#
	#########################################
	
	public function get_modules()
	{
		$this->db->select('`m`.`ID`, `m`.`name` AS `ModuleName`, CONCAT(`s`.`firstName`, " ", `s`.`lastName`) AS `Lecturer`', FALSE)
		         ->select('`cm`.`year`, CONCAT(`ct`.`type`, " ", `c`.`name`) AS `Course`', FALSE)
				 ->from('tblModules `m`, tblStaff `s`, tblCourseModules `cm`, tblCourses `c`, tblCourseType `ct`')
				 ->where('`m`.`taught_by` = `s`.`staff_id`')
				 ->where('`c`.`ID` = `cm`.`course_id`')
				 ->where('`m`.`ID` = `cm`.`module_id`')
				 ->where('`ct`.`ID` = `c`.`type`')
				 ->order_by('`m`.`ID`');
		
		$query = $this->db->get();
		return $query->result_array();
	}
	
	#########################################
	#	Get module							#
	#	public								#
	#########################################
	
	public function get_module($id)
	{
		$this->db->select()
				 ->from('tblModules')
				 ->where('ID',$id);
		$query = $this->db->get();
		
		// returns an array of objects - we want the first one
		$m = $query->result();
		return $m[0];;
	}
	
	#########################################
	#	Get module course ID				#
	#	public								#
	#########################################
	
	public function get_module_course($id)
	{
		$this->db->select('course_id')
				 ->from('tblCourseModules')
				 ->where('module_id',$id);
		$query = $this->db->get();
		
		$result = $query->row_array();
		return $result['course_id'];
	}
	
	#########################################
	#	Delete module						#
	#	public								#
	#########################################
	
	public function delete_module($id, $trans = 1)
	{
		// done in this order to preserve foreign keys etc
		
		// using transactions? (when this method is called in other methods using transactions, transactions must NOT be used
		if($trans === 1)
			$this->db->trans_begin();
		
		// delete assignment marks
		$this->db->query("DELETE FROM `tblAssignmentMarks`
							USING `tblAssignments`, `tblAssignmentMarks`
							WHERE `tblAssignments`.`module_id` = " . $id . "
							AND `tblAssignmentMarks`.`assignment_id` = `tblAssignments`.`ID`");
		
		// delete assignment submission files for that module
		$this->db->select('`as`.`location` AS `loc`', FALSE)
				 ->from('tblAssignmentSubmissions `as`, tblAssignments `a`')
				 ->where('`as`.`assignment_id` = `a`.`ID`')
				 ->where('`a`.`module_id`', $id);

		$query = $this->db->get();
		$submissions = $query->result_array();
		
		
		// delete assignment submission DB entries
		// this WOULD work, but there is a bug in mysql
		$this->db->query("DELETE FROM `tblAssignmentSubmissions`
							USING `tblAssignmentSubmissions`, `tblAssignments`
							WHERE `tblAssignments`.`module_id` = '".$id."'
							AND `tblAssignmentSubmissions`.`assignment_id` = `tblAssignments`.`ID`");
		
		
		// delete assignment briefs
		$this->db->select('specification_file')
				 ->from('tblAssignments')
				 ->where('module_id', $id);
		
		$query = $this->db->get();
		$briefs = $query->result_array();

		// delete assignments
		
		$this->db->where('module_id', $id)
				 ->delete('tblAssignments');
		
		// delete module from CourseModules
		
		$this->db->where('module_id', $id)
				 ->delete('tblCourseModules');
		
		// delete module
		$this->db->where('ID',$id)
				 ->delete('tblModules');
	
		if($trans === 1)
		{	
			if($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
				return false;
			}
			else
			{
				// commit transaction
				$this->db->trans_commit();
				
				// delete files
				foreach($briefs as $f)
				{
					shell_exec('rm /home/jrdn/hnd/www/uploads/staff/' . $f['specification_file']);
				}
				
				foreach($submissions as $f)
				{
					shell_exec('rm ' . $f['loc']);
				}
				
				return true;
			}
		}
		else
		{
			return true;
		}
	}
	
	#########################################
	#	Edit module							#
	#	public								#
	#########################################
	
	public function edit_module($id, $module, $lecturer, $course)
	{
		// start transaction as we're running two queries...
		
		$this->db->trans_begin();
		
		$data	= array(
					'name' => $module,
					'taught_by' => $lecturer
						);
		
		$this->db->where('ID', $id)
			 	 ->update('tblModules', $data);
		
		$data	= array(
					'course_id' => $course
						);
		
		$this->db->where('module_id', $id)
				 ->update('tblCourseModules', $data);
				 
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return false;	
		}
		else
		{
			$this->db->trans_commit();
			return true;
		}
	}
	
	#########################################
	#	Add module							#
	#	public								#
	#########################################
	
	public function add_module($name, $taught_by, $course, $year)
	{
		// transaction
		$this->db->trans_begin();
		
		// insert into tblModules
		$data = array(
					'name' => $name,
					'taught_by' => $taught_by
					);
		$this->db->insert('tblModules', $data);
		
		$data = array(
					'course_id' => $course,
					'module_id' => $this->db->insert_id(),
					'year' => $year
					);
		
		$this->db->insert('tblCourseModules', $data);
		
		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return false;
		}
		else
		{
			$this->db->trans_commit();
			return true;
		}
	}
}

/* End of File */
/* File Location: ./application/models/Modules.php */