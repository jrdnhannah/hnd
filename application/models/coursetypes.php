<?php if( ! defined('BASEPATH') ) exit('No direct script access!');

class CourseTypes extends CI_Model
{
	#########################################
	#	Add Course Type						#
	#	public								#
	#########################################
	
	public function add_course_type($type, $marks)
	{
		$marks = str_replace("\n", ",", $marks);
		
		$data = array(
					'type' => $type,
					'marks' => $marks
					);
		
		if($this->db->insert('tblCourseType', $data))
			return true;
		else
			return false;
	}
	
	#########################################
	#	Get Course Types					#
	#	public								#
	#########################################
	
	public function get_course_types()
	{
		$this->db->select()
				 ->from('tblCourseType')
				 ->order_by('type');
		
		$query = $this->db->get();
		
		return $query->result();
	}
	
	#########################################
	#	Get Course Type						#
	#	public								#
	#########################################
	
	public function get_course_type($id)
	{
		$this->db->select()
				 ->from('tblCourseType')
				 ->where('ID',$id)
				 ->limit(1);
		
		$query = $this->db->get();
		
		return $query->result();
	}
	
	#########################################
	#	Edit Course Type					#
	#	public								#
	#########################################
	
	public function update_course_type($id, $type, $marks)
	{
		$marks = str_replace("\n", ",", $marks);
		
		$data = array(
					'type' => $type,
					'marks' => $marks
					);
		
		$this->db->where('ID', $id);
		
		if($this->db->update('tblCourseType',$data))
			return true;
		else
			return false;
	}
	
	#########################################
	#	Delete Course Type					#
	#	public								#
	#########################################
	
	public function delete_course_type($id, $trans = 1)
	{
		// using transactions? (when this method is called in other methods using transactions, transactions must NOT be used
		if($trans === 1)
			$this->db->trans_begin();
		
		// got to delete all modules within a course, so might as well use the modules model
		$ci =& get_instance();
		$ci->load->model('courses');
		
		// get all the courses from tblCourses
		$this->db->select('ID')
				 ->from('tblCourses')
				 ->where('type', $id);
		
		$query = $this->db->get();
		
		// delete the module
		foreach($query->result() as $c)
		{
			$this->courses->delete_course($c->ID, 0);
		}
		
		// delete department
		$this->db->where('ID',$id)
				 ->delete('tblCourseType');
		
		if($trans === 1)
		{
			if($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
				return false;
			}
			else
			{
				$this->db->trans_commit();
				return true;
			}
		}
		else
		{
			return true;	
		}
	}
}

/* End of File */
/* File Location: ./application/models/Departments.php */