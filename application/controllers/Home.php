<?php if( ! defined('BASEPATH')) exit('No direct script access!');

// meow
class Home extends CI_Controller
{
	public function index()
	{
		// check if user is logged in, and if they are, redirect to relevant portal
		if(isset($_COOKIE['userdata']))
		{
			$this->load->library(array('StudentOperations', 'StaffOperations', 'NetworkAdminOperations'));
			if($this->studentoperations->is_logged_in()) header('Location: ./StudentPortal.htm');
			elseif($this->staffoperations->is_logged_in()) header('Location: ./StaffPortal.htm');
			elseif($this->networkadminoperations->is_logged_in()) header('Location: ./NetworkAdminPortal.htm');
		}
		
		// set up variables
		$data['title'] = 'Log In';
		
		// load relevant helper/library files
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		// set some validation rules
		// the form uses HTML5 'required' fields, but it's best to have something server side to fall back on.
		$this->form_validation->set_rules('pnumber', 'P Number', 'required|callback__valid_id');
		$this->form_validation->set_rules('password', 'Password', 'required|callback__valid_account');
		$this->form_validation->set_rules('type', 'Login Type', 'required');

		
		
		if($this->form_validation->run() == false)
		{
			$this->load->view('templates/header', $data);
			
			$this->load->view('forms/login');
			
			$this->load->view('templates/footer');
		}
		else
		{
			// we already know that the details supplied are valid, thanks to our callback functions
			// but making extra sure won't hurt, in case false $_POST information was sent.
			$pass = $_POST['password'];
			
			if( ! $this->_valid_id($_POST['pnumber']) || ! $this->_valid_account($pass)) return;
	
			// work out if we're dealing with a student or staff member, and create object accordingly
			if($_POST['type'] == 'student')
			{
				$this->load->library('StudentOperations');
				$this->studentoperations->LogIn($_POST['pnumber'], md5($pass));
			}
			else if ($_POST['type'] == 'staff')
			{
				$this->load->library('StaffOperations');
				$this->staffoperations->LogIn($_POST['pnumber'], md5($pass));
			}
			else if ($_POST['type'] == 'nwadmin')
			{
				$this->load->library('NetworkAdminOperations');
				$this->networkadminoperations->LogIn($_POST['pnumber'], md5($pass));
			}
			
		}
	}
	
	public function LogOut()
	{
		setcookie('userdata', '', '0', '/', '.hnd.wp7solutions.co.uk');
		header('Location: ../');	
	}
	
	#########################################
	#	Check if user id is valid			#
	#	Private								#
	#########################################
	
	public function _valid_id($str)
	{
		// this function is only called when there is post data
		// while the type is stated explicitly, running SQL queries straight from the post data
		// can expose the website to SQL injection attacks.
		// even though the site is using Active Record, which escapes strings, best to minimize risk as much as possible!
		
		if($_POST['type'] == 'student')
		{
			$this->load->library('StudentOperations');
			$valid = $this->studentoperations->is_valid_id($str);
		}
		elseif($_POST['type'] == 'staff')
		{
			$this->load->library('StaffOperations');
			$valid = $this->staffoperations->is_valid_id($str);
		}
		elseif($_POST['type'] == 'nwadmin')
		{
			$this->load->library('NetworkAdminOperations');
			$valid = $this->networkadminoperations->is_valid_id($str);
		}
		
		if($valid == true)
		{
			return true;
		}
		else
		{	
			// set error message
			$this->form_validation->set_message('_valid_id', 'Your username or password is incorrect.');
			return false;
		}
	}
	
	#############################################
	#	Check if user id matches pass is valid	#
	#	Private									#
	#############################################
	
	public function _valid_account($pass)
	{
		// if we've already established the username is bad, exit silently
		if(!$this->_valid_id($_POST['pnumber']))
		{
			$this->form_validation->set_message('_valid_account', '');
			return false;
		}
		
		if($_POST['type'] == 'student')
		{
			$this->load->library('StudentOperations');
			$valid = $this->studentoperations->is_valid_account($_POST['pnumber'], md5($pass));
		}
		elseif($_POST['type'] == 'staff')
		{
			$this->load->library('StaffOperations');
			$valid = $this->staffoperations->is_valid_account($_POST['pnumber'], md5($pass));
		}
		elseif($_POST['type'] == 'nwadmin')
		{
			$this->load->library('NetworkAdminOperations');
			$valid = $this->networkadminoperations->is_valid_account($_POST['pnumber'], md5($pass));
		}
			
		if($valid == true)
		{
			return true;
		}
		else
		{
			// set error message
			$this->form_validation->set_message('_valid_account', 'Your username or password is incorrect.');
			return false;
		}
	}
}


/* End of File */
/* File Location: ./application/controlls/Home.php */