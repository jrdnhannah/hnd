<?php if ( ! defined('BASEPATH') ) exit('No direct script access!');


class StaffPortal extends CI_Controller
{
	private $staff;
		
	public function __construct()
	{
		parent::__construct();
		// load url/form helper - will be used by several methods
		$this->load->helper(array('form', 'url'));
		
		// if user is not logged in, return to home!
		$this->load->library('StaffOperations');
		if($this->staffoperations->is_logged_in() == false)
			header('Location: '.site_url().'Home.htm');
		
		// Serialize the cookie and store as an object
		require_once(APPPATH.'libraries/Staff.php');
		$this->staff = unserialize(base64_decode($_COOKIE['userdata']));
	}
	
	public function index()
	{
		// set up some basic data to be loaded into views
		$data['title']	= "Staff Portal";
		$data['name']	= ucfirst($this->staff->data['firstname']) . ' ' . ucfirst($this->staff->data['lastname']);
		$data['dept']	= ucfirst($this->staffoperations->get_dept_name($this->staff->data['dept']));
		
		// set up the table
		// load table library
		$this->load->library('table');
		
		// set caption
		$this->table->set_caption('Modules I Teach');
		
		// set table headings
		$this->table->set_heading('Module', 'Course', 'Year', 'Assignments', 'Add New Assignment');
		
		// table template
		$this->table->set_template(array('table_open' => '<table class="table table-striped">'));
		
		$tabledata = $this->staffoperations->get_modules($this->staff->data['id']);
		
		// create table
		for($i = 0; $i < count($tabledata); $i++)
		{
			
			$assLink = '<div class="btn-toolbar">';
			$assLink .= '<div class="btn-group">';
			$assLink .= '<a href="#" class="btn btn-primary dropdown-toggle">Assignments&nbsp;';
			$assLink .= '<span class="caret"></span>';
			$assLink .= '</a>';
			$assLink .= '<ul class="dropdown-menu">';
			
			
			$assignments = $this->staffoperations->get_assignments($tabledata[$i]['moduleID'], $tabledata[$i]['courseID'], $this->staff->data['id']);
			
			// if there are no assignments set yet, inform staff of that
			if(count($assignments) == 0)
			{
				$assLink .= '<li>No assignments have been listed for this module at this time.</li>';
			}
			// other wise, fill 'er up!
			else
			{
				for($x = 0; $x < count($assignments); $x++)
				{
					// Outputs links like: /StaffPortal/Assignment/COURSE_ID/MODULE_ID/ASSIGNMENT_ID.htm
					$assLink .= '<li><a href="./StaffPortal/Assignment/'.$assignments[$x]['courseID'].'/'.$assignments[$x]['moduleID'].'/'.$assignments[$x]['assignmentID'].'.htm">'.$assignments[$x]['name'].'<br />'.$assignments[$x]['duedate'].'</a></li>';
				}
			}
			
			$assLink .= '</ul>';
			$assLink .= '</div>';
			$assLink .= '</div>';
			
			$addLink = '<div class="btn-toolbar">';
			$addLink .= '<div class="btn-group">';
			
			$addLink .= '<a href="./StaffPortal/CreateAssignment/'.$tabledata[$i]['courseID'].'/'.$tabledata[$i]['moduleID'].'.htm" class="btn btn-primary">Add Assignment&nbsp;';
			$addLink .= '<i class="icon-plus icon-white"></i>';
			$addLink .= '</a>';
			
			$addLink .= '</div>';
			$addLink .= '</div>';
			
			$this->table->add_row($tabledata[$i]['modName'], $tabledata[$i]['courseName'], $tabledata[$i]['year'], $assLink, $addLink);
		}
		
		$data['table'] = $this->table->generate();
		
		$this->load->view('templates/header', $data);
		
		$this->load->view('staff/templates/heading', $data);
		
		$this->load->view('staff/portal', $data);
		
		$this->load->view('templates/footer');
	}
	
	public function CreateAssignment($cid, $mid)
	{
		$data['title'] = 'Create Assignment';
		$data['name']	= ucfirst($this->staff->data['firstname']) . ' ' . ucfirst($this->staff->data['lastname']);
		$data['dept']	= ucfirst($this->staffoperations->get_dept_name($this->staff->data['dept']));
		
		$this->load->view('templates/header', $data);
		
		$this->load->view('staff/templates/heading', $data);
		
		// load form validation lib
		$this->load->library('form_validation');
		// set config for validation
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('percentage', 'Percentage', 'required');
		$this->form_validation->set_rules('date', 'Date', 'required|callback__is_valid_date');
		$this->form_validation->set_rules('types', 'File Types', 'required');

		// load file uploading lib
		$this->load->library('upload');
		// config the upload lib
		$config['upload_path'] = './uploads/staff';
		$config['allowed_types'] = '*';
		
		$this->upload->initialize($config);

		if( ! $this->form_validation->run() )
		{
		
			$this->load->view('staff/forms/createassignment');
		
		}
		else
		{
			$this->upload->do_upload('brief');
			// upload succeeded, test the file for errors
			if($this->_virusscan($this->upload->data() == 1))
			{
				$data['error'] = 'virus';
				
				$this->load->view('staff/error', $data);
			}
			// invalid extension
			else if( ! $this->_is_valid_ext($this->upload->data()))
			{
				$data['error'] = 'ext';
				
				$this->load->view('staff/error', $data);
			}
			// everything is valid
			else
			{
				$file = $this->_rename_file($this->upload->data(), $cid, $mid);
				if($this->staffoperations->add_assignment($file, $mid))
				{
					$this->load->view('staff/uploadsuccess');
				}
			}
		}
		
		$this->load->view('templates/footer');
	}
	
	public function Assignment($cid, $mid, $aid)
	{
		$data['title'] = 'View Assignment';
		$data['name']	= ucfirst($this->staff->data['firstname']) . ' ' . ucfirst($this->staff->data['lastname']);
		$data['dept']	= ucfirst($this->staffoperations->get_dept_name($this->staff->data['dept']));
		$data['assName'] = $this->staffoperations->get_assignment_name($aid);
		$data['date'] = $this->staffoperations->get_assignment_duedate($aid);
		
		// lets create a table
		$this->load->library('table');
		$this->table->set_caption('Assignment Submissions');
		$this->table->set_heading('Name', 'PNumber', 'View Assignment', 'Mark Assignment');
		// table template
		$this->table->set_template(array('table_open' => '<table class="table table-striped">'));
		
		$tabledata = $this->staffoperations->get_submitted_assignments($cid, $mid, $aid);
		
		for($i = 0; $i < count($tabledata); $i++)
		{
			$file = explode('/', $tabledata[$i]['location']);
			$file = $file[count($file) - 1];
			
			$link = '<div class="btn-toolbar">';
			$link .= '<div class="btn-group">';
			
			$link .= '<a href="./uploads/students/'.$file.'" class="btn btn-primary">Download Assignment&nbsp;';
			$link .= '<i class="icon-download-alt icon-white"></i>';
			$link .= '</a>';
			
			$link .= '</div>';
			$link .= '</div>';
			
			$marklink  = '<div class="btn-toolbar">';
			$marklink .= '<div class="btn-group">';
			
			if($this->staffoperations->is_marked($tabledata[$i]['pnumber'], $aid))
				$marklink .= '<a href="'.current_url().'" class="btn btn-primary disabled">Marked&nbsp;';	
			else
				$marklink .= '<a href="./StaffPortal/MarkAssignment/'.$tabledata[$i]['pnumber'].'/'.$aid.'.htm" class="btn btn-primary">Mark Assignment&nbsp;';
				
			$marklink .= '<i class="icon-pencil icon-white"></i>';
			$marklink .= '</a>';
			$marklink .= '</div>';
			$marklink .= '</div>';
			
			$this->table->add_row($tabledata[$i]['name'], $tabledata[$i]['pnumber'], $link, $marklink);
		}
		
		$data['table'] = $this->table->generate();
		
		$this->load->view('templates/header', $data);
		$this->load->view('staff/templates/heading', $data);
		
		$this->load->view('staff/assignments', $data);
		
		$this->load->view('templates/footer');
		
	}
	
	public function MarkAssignment($pnumber, $aid)
	{
		// set up data
		$data['title']	= 'Mark Assignment';
		$data['name']	= ucfirst($this->staff->data['firstname']) . ' ' . ucfirst($this->staff->data['lastname']);
		$data['dept']	= ucfirst($this->staffoperations->get_dept_name($this->staff->data['dept']));
		
		// include templates & other views
		$this->load->view('templates/header', $data);
		$this->load->view('staff/templates/heading', $data);
		
		// load form validation lib
		$this->load->library('form_validation');
		// there are no "greater than or equal" etc rules in CI, so lets improvise!
		$this->form_validation->set_rules('percentage', 'Percentage', 'required|greater_than[-1]|less_than[101]|integer');
		$this->form_validation->set_rules('grades', 'Grade', 'required');
		$this->form_validation->set_rules('feedback', 'Feedback', 'required');
		
		if($this->form_validation->run() == FALSE)
		{
		
			// work out what type of course student is on (HND/BSc/etc) and provide grade selection accordingly
			$course			= $this->staffoperations->get_course_type($pnumber);
			$data['grades']	= $this->staffoperations->get_course_marks($course);
			$data['assignment'] = $this->staffoperations->get_assignment_name($aid);
			$data['student'] = $this->staffoperations->get_student_name($pnumber);
	
			$this->load->view('staff/forms/markassignment', $data);

		}
		else
		{
			$student	= $pnumber;
			$assignment	= $aid;
			$submission = $this->staffoperations->get_assignment_submission_id($pnumber,$aid);
			$percentage = $this->input->post('percentage');
			$grade		= ucfirst($this->input->post('grades'));
			$feedback	= $this->input->post('feedback');
			
			if($this->staffoperations->mark_assignment($student, $assignment, $submission, $grade, $percentage, $feedback))
			{
				$this->load->view('staff/marksuccess');
			}
			else
			{
				$this->load->view('staff/markfailure');
			}
		}
		
		$this->load->view('templates/footer');
	}
	
	#########################################
	#	Scan file for viruses				#
	#	private								#
	#########################################
	
	private function _virusscan($file)
	{
		// executes virus scan on file, and removes if it detects a positive
		// outputs return value into $int
		exec('clamdscan '.$file['full_path'] . ' --remove', $out, $int);
		return $int;		
	}
	
	#########################################
	#	Is the date supplied valid?			#
	#	private								#
	#########################################
	
	public function _is_valid_date($date)
	{
		if(strtotime($date) == false)
		{
			$this->form_validation->set_message('_is_valid_date', 'The date supplied is not valid. Please enter in the format: YYYY-MM-DD');
			return false;	
		}
		else
		{
			return true;
		}
	}
	
	#########################################
	#	rename the file so that other files #
	#	with the same uploaded name don't	#
	#	overwrite it						#
	#	private								#
	#########################################
	
	private function _rename_file($file, $cid, $mid)
	{
		$filepath	= $file['file_path'];
		$filename	= $file['file_name'];
		
		// get file extension
		$ext		= explode('.', $filename);
		$ext		= $ext[count($ext) - 1];
		
		$salt		= rand(100, 20000);
		$newname	= $this->staff->data['id'] . $cid . $mid . sha1($filename) . $salt . '.' . $ext;
		
		// rename the file using ubuntu shell
		shell_exec('mv ' . $filepath . $filename . ' ' . $filepath . $newname);
		
		// return path and name
		return $newname;
	}
	
	#########################################
	#	Is file valid ext?					#
	#	private								#
	#########################################
	
	private function _is_valid_ext($file)
	{
		// even though the users of this page are staff, they may still upload undesired files, by accident, or they could have their account hacked.
		$types = 'doc,docx,odt';
		// put the file types into an array
		$types = explode(",", $types);
		
		// get file extension
		$ext	= explode('.', $file['file_name']);
		// make $ext the last element of $ext
		$ext	= $ext[count($ext) - 1];
		
		$valid = false;
		
		foreach($types as $value)
		{
			// if the extension matches a valid one, set $valid to true and exit loop
			if($ext == $value)
			{
				$valid = true;
				break;	
			}
		}
		
		// if file is invalid, remove it
		if($valid == false)
		{
			exec('rm ' . $file['full_path']);
		}
			
		return $valid;
	}
}

/* End of File */
/* File Location: ./application/controllers/StaffPortal.php */