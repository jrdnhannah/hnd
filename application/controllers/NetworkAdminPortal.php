<?php if( ! defined('BASEPATH') ) exit('No direct script access!');

class NetworkAdminPortal extends CI_Controller
{
	private $admin;
		
	public function __construct()
	{
		parent::__construct();
		// load url/form helper - will be used by several methods
		$this->load->helper(array('form', 'url'));
		$this->load->library('table');
		
		// if user is not logged in, return to home!
		$this->load->library('NetworkAdminOperations');
		if($this->networkadminoperations->is_logged_in() == false)
			header('Location: '.site_url().'Home.htm');
		
		// Serialize the cookie and store as an object
		require_once(APPPATH.'libraries/NetworkAdmin.php');
		$this->admin = unserialize(base64_decode($_COOKIE['userdata']));
	}
	
	public function index()
	{
		$data['title'] = 'Network Administration Portal';
		
		$data['name'] = $this->admin->data['firstname'] . ' ' . $this->admin->data['lastname'];
		
		$this->load->view('templates/header', $data);
		$this->load->view('nwa/nav.php');
		
		$this->load->view('nwa/templates/heading', $data);
		
		$this->load->view('templates/footer');
	}
	
	#########################################
	#	Student Management					#
	#########################################
	
	public function BrowseStudents($offset = 0)
	{
		
		$data['title'] = 'Manage Students';
		$data['name'] = $this->admin->data['firstname'] . ' ' . $this->admin->data['lastname'];
		
		$this->load->view('templates/header', $data);
		$this->load->view('nwa/nav');
		
		$this->load->view('nwa/templates/heading', $data);
		
		$this->load->model('Student_UserDB');
		// view student list
		$students = $this->Student_UserDB->get_students($offset);
		
		// set table headings
		$this->table->set_heading('PNumber', 'Name', 'Course Dates', 'Year', 'Actions');
		$this->table->set_template(array('table_open' => '<table class="table table-striped table-bordered span6">'));
		
		foreach($students as $s)
		{
			$actions  = '<div class="btn-toolbar">';
			$actions .= '<div class="btn-group">';
			
			$actions .= '<a href="#" class="btn btn-primary dropdown-toggle"><i class="icon-ok icon-white"></i> Actions...</a>';
			
			$actions .= '<ul class="dropdown-menu">';
			
			$actions .= '<li><a href="'.site_url().'NetworkAdminPortal/EditStudent/'.$s->pnumber.'.htm" class="alert-success"><i class="icon-pencil"></i> Edit Student</a></li>';
			$actions .= '<li><a href="'.site_url().'NetworkAdminPortal/DeleteStudent/'.$s->pnumber.'.htm" class="alert-danger"><i class="icon-trash"></i> Delete Student</a></li>';
			
			$actions .= '</ul>';
			
			$actions .= '</div>';
			$actions .= '</div>';
			
			$this->table->add_row($s->pnumber, $s->firstName . ' ' . $s->lastName, 'Start: ' . $s->startdate . '<br />End: ' . $s->enddate, $s->year, $actions);
		}
		
		$data['table'] = $this->table->generate();
		
		$this->load->view('nwa/students/studentlist', $data);
		
		
		// add student
		
		$this->load->view('templates/footer');
	}
	
	public function AddStudent()
	{
		// default tings
		$data['title']	= 'Add Student';
		$data['name']	= $this->admin->data['firstname'] . ' ' . $this->admin->data['lastname'];
		
		$this->load->view('templates/header', $data);
		$this->load->view('nwa/nav');
		
		$this->load->view('nwa/templates/heading', $data);
		
		// form tings
		$this->load->model(array('Student_UserDB', 'courses'));
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		// set validation rules
		$this->form_validation->set_rules('pnumber', 'PNumber', 'is_unique[tblStudents.pnumber]');
		$this->form_validation->set_rules('firstname', 'First Name', 'required');
		$this->form_validation->set_rules('lastname', 'Last Name', 'required');
		$this->form_validation->set_rules('gender', 'Gender', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('password2', 'Password Confirmation', 'required|matches[password]');
		$this->form_validation->set_rules('course', 'Course', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('address', 'Address', 'required');
		$this->form_validation->set_rules('startdate', 'Start Date', 'required');
		$this->form_validation->set_rules('enddate', 'End Date', 'required');
		$this->form_validation->set_rules('year', 'year', 'required|is_natural_no_zero');
		
		if($this->form_validation->run() == FALSE)
		{
			$data['courses'] = $this->courses->get_course_list();
						
			$this->load->view('nwa/forms/addstudent', $data);
		}
		else
		{
			$id			= $this->input->post('pnumber');
			$firstname	= $this->input->post('firstname');
			$lastname	= $this->input->post('lastname');
			$password	= $this->input->post('password');
			$gender		= $this->input->post('gender');
			$address	= $this->input->post('address');
			$course		= $this->input->post('course');
			$email		= $this->input->post('email');
			$startdate	= $this->input->post('startdate');
			$enddate	= $this->input->post('enddate');
			$year	= $this->input->post('year');
			
			if($this->Student_UserDB->add_student($id, $firstname, $lastname, $password, $gender, $address, $email, $course, $startdate, $enddate, $year))
			{
				$data['msg'] = 'The student was successfully added.';
				$this->load->view('nwa/templates/success', $data);
			}
			else
			{
				$this->load->view('nwa/templates/failure');
			}
		}
	}
	
	public function EditStudent($id)
	{
		// load default stuff
		$data['title']	= 'Edit Student';
		$data['name']	= $this->admin->data['firstname'] . ' ' . $this->admin->data['lastname'];
		
		$this->load->view('templates/header', $data);
		$this->load->view('nwa/nav');
		
		$this->load->view('nwa/templates/heading', $data);
		
		// formy wormy
		$this->load->model(array('Student_UserDB', 'courses'));
		$this->load->library('form_validation');
		
		// set validation rules
		$this->form_validation->set_rules('firstname', 'First Name', 'required');
		$this->form_validation->set_rules('lastname', 'Last Name', 'required');
		$this->form_validation->set_rules('gender', 'Gender', 'required');
		$this->form_validation->set_rules('course', 'Course', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required');
		$this->form_validation->set_rules('startdate', 'Start Date', 'required');
		$this->form_validation->set_rules('enddate', 'End Date', 'required');
		
		if($this->form_validation->run() == FALSE)
		{
			$data['student'] = $this->Student_UserDB->get_student($id);
			$data['courses'] = $this->courses->get_course_list();
						
			$this->load->view('nwa/forms/editstudent', $data);
		}
		else
		{
			$firstname	= $this->input->post('firstname');
			$lastname	= $this->input->post('lastname');
			$password	= $this->input->post('password') == '' ? NULL : $this->input->post('password');
			$gender		= $this->input->post('gender');
			$address	= $this->input->post('address');
			$course		= $this->input->post('course');
			$email		= $this->input->post('email');
			$startdate	= $this->input->post('startdate');
			$enddate	= $this->input->post('enddate');
			
			if($this->Student_UserDB->update_student($id, $firstname, $lastname, $password, $gender, $address, $email, $course, $startdate, $enddate))
			{
				$data['msg'] = 'The student was successfully updated.';
				$this->load->view('nwa/templates/success', $data);
			}
			else
			{
				$this->load->view('nwa/templates/failure');
			}
		}
		
		$this->load->view('templates/footer');
		
	}
	
	public function DeleteStudent($id)
	{
		// load default stuff
		$data['title']	= 'Edit Student';
		$data['name']	= $this->admin->data['firstname'] . ' ' . $this->admin->data['lastname'];
		
		$this->load->view('templates/header', $data);
		$this->load->view('nwa/nav');
		
		$this->load->view('nwa/templates/heading', $data);
		
		$this->load->model('Student_UserDB');
		
		if($this->Student_UserDB->delete_student($id))
		{
			$data['msg'] = 'The student was successfully deleted from the system.';
			$this->load->view('nwa/templates/success', $data);
		}
		else
		{
			$this->load->view('nwa/templates/failure');
		}
		
		$this->load->view('templates/footer');
	}
	
	#########################################
	#	Course Management					#
	#########################################
	
	public function AddCourse()
	{
		// load default stuff
		$data['title']	= 'Add Course';
		$data['name']	= $this->admin->data['firstname'] . ' ' . $this->admin->data['lastname'];
		
		$this->load->view('templates/header', $data);
		$this->load->view('nwa/nav');
		
		$this->load->view('nwa/templates/heading', $data);
		
		// formy wormy
		$this->load->library('form_validation');
		$this->load->model(array('courses', 'departments', 'coursetypes'));
		
		$this->form_validation->set_rules('coursetype', 'Course Type', 'required');
		$this->form_validation->set_rules('coursename', 'Course Name', 'required');
		$this->form_validation->set_rules('department', 'Department', 'required');
		
		if($this->form_validation->run() == FALSE)
		{
			$data['coursetypes'] = $this->coursetypes->get_course_types();
			$data['departments'] = $this->departments->get_departments();
			
			$this->load->view('nwa/forms/addcourse.php', $data);
		}
		else
		{
			$type = $this->input->post('coursetype');
			$name = $this->input->post('coursename');
			$dept = $this->input->post('department');
			$desc = $this->input->post('description');
			
			if($this->courses->add_course($type, $name, $dept, $desc))
			{
				$data['msg'] = "Course successfully added.";
				$this->load->view('nwa/templates/success', $data);
			}
			else
			{
				$this->load->view('nwa/templates/failure');
			}
		}
		
		$this->load->view('templates/footer');
	}
	
	public function BrowseCourses()
	{
		// load default stuff
		$data['title']	= 'Browse Courses';
		$data['name']	= $this->admin->data['firstname'] . ' ' . $this->admin->data['lastname'];
		
		$this->load->view('templates/header', $data);
		$this->load->view('nwa/nav');
		
		$this->load->view('nwa/templates/heading', $data);
		
		// tabley stuff
		$this->load->model('courses');
		$this->load->library('table');
		$this->table->set_template(array('table_open' => '<table class="table table-striped table-bordered span6">'));
		$this->table->set_heading('Course Name', 'Actions');
		
		foreach($this->courses->get_course_list_with_department_names() as $course)
		{
			
			$actions  = '<div class="btn-toolbar">';
			$actions .= '<div class="btn-group">';
			
			$actions .= '<a href="#" class="btn btn-primary dropdown-toggle"><i class="icon-ok icon-white"></i> Actions...</a>';
			
			$actions .= '<ul class="dropdown-menu">';
			
			$actions .= '<li><a href="'.site_url().'NetworkAdminPortal/EditCourse/'.$course->ID.'.htm" class="alert-success"><i class="icon-pencil"></i> Edit Course</a></li>';
			$actions .= '<li><a href="'.site_url().'NetworkAdminPortal/DeleteCourse/'.$course->ID.'.htm" class="alert-danger"><i class="icon-trash"></i> Delete Course</a></li>';
			
			$actions .= '</ul>';
			
			$actions .= '</div>';
			$actions .= '</div>';
			
			$this->table->add_row($course->coursename . '<br />' . $course->department, $actions);
		}
		
		$data['table'] = $this->table->generate();
		
		$this->load->view('nwa/courses/browsecourses', $data);
		
		$this->load->view('templates/footer');
	}
	
	public function EditCourse($id)
	{
		// load default stuff
		$data['title']	= 'Edit Course';
		$data['name']	= $this->admin->data['firstname'] . ' ' . $this->admin->data['lastname'];
		
		$this->load->view('templates/header', $data);
		$this->load->view('nwa/nav');
		
		$this->load->view('nwa/templates/heading', $data);
		
		// formy wormy
		$this->load->model(array('departments', 'courses', 'coursetypes'));
		$this->load->library('form_validation');
		
		// set validation rules
		$this->form_validation->set_rules('type', 'Course Type', 'required');
		$this->form_validation->set_rules('name', 'Course Name', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');
		$this->form_validation->set_rules('department', 'Department', 'required');
		
		if($this->form_validation->run() == FALSE)
		{
			// only need one course, but the object is returned in an array
			$course = $this->courses->get_course($id);
			$data['course'] = $course[0];
			
			$data['types']	= $this->coursetypes->get_course_types();
			$data['depts']	= $this->departments->get_departments();
		
						
			$this->load->view('nwa/forms/editcourse', $data);
		}
		else
		{
			$type			= $this->input->post('type');
			$name			= $this->input->post('name');
			$description	= $this->input->post('description');
			$department		= $this->input->post('department');
			
			if($this->courses->update_course($id, $type, $name, $description, $department))
			{
				$data['msg'] = 'The course was successfully updated.';
				$this->load->view('nwa/templates/success', $data);
			}
			else
			{
				$this->load->view('nwa/templates/failure');
			}
		}
		
		$this->load->view('templates/footer');
	}
	
	public function DeleteCourse($id)
	{
		// load default stuff
		$data['title']	= 'Delete Course';
		$data['name']	= $this->admin->data['firstname'] . ' ' . $this->admin->data['lastname'];
		
		$this->load->view('templates/header', $data);
		$this->load->view('nwa/nav');
		
		$this->load->view('nwa/templates/heading', $data);
		
		$this->load->model('courses');
		
		if($this->courses->delete_course($id))
		{
			$data['msg'] = 'The course and associated data was successfully deleted from the system.';
			$this->load->view('nwa/templates/success', $data);
		}
		else
		{
			$this->load->view('nwa/templates/failure');
		}
		
		$this->load->view('templates/footer');
	}
	
	#########################################
	#	Course Type Management				#
	#########################################
	
	public function AddCourseType()
	{
		// load default stuff
		$data['title']	= 'Add Course Type';
		$data['name']	= $this->admin->data['firstname'] . ' ' . $this->admin->data['lastname'];
		
		$this->load->view('templates/header', $data);
		$this->load->view('nwa/nav');
		
		$this->load->view('nwa/templates/heading', $data);
		
		// formy wormy
		$this->load->library('form_validation');
		$this->load->model(array('coursetypes'));
		
		$this->form_validation->set_rules('coursetype', 'Course Type', 'required');
		$this->form_validation->set_rules('coursemarks', 'Course Marks', 'required');
		
		if($this->form_validation->run() == FALSE)
		{
			$this->load->view('nwa/forms/addcoursetype.php', $data);
		}
		else
		{
			$type  = $this->input->post('coursetype');
			$marks = $this->input->post('coursemarks');
			
			if($this->coursetypes->add_course_type($type, $marks))
			{
				$data['msg'] = "Course type successfully added.";
				$this->load->view('nwa/templates/success', $data);
			}
			else
			{
				$this->load->view('nwa/templates/failure');
			}
		}
		
		$this->load->view('templates/footer');
	}
	
	public function BrowseCourseTypes()
	{
		// load default stuff
		$data['title']	= 'Browse Course Types';
		$data['name']	= $this->admin->data['firstname'] . ' ' . $this->admin->data['lastname'];
		
		$this->load->view('templates/header', $data);
		$this->load->view('nwa/nav');
		
		$this->load->view('nwa/templates/heading', $data);
		
		// tabley stuff
		$this->load->model('coursetypes');
		$this->load->library('table');
		$this->table->set_template(array('table_open' => '<table class="table table-striped table-bordered span6">'));
		$this->table->set_heading('Course Type', 'Actions');
		
		foreach($this->coursetypes->get_course_types() as $type)
		{
			
			$actions  = '<div class="btn-toolbar">';
			$actions .= '<div class="btn-group">';
			
			$actions .= '<a href="#" class="btn btn-primary dropdown-toggle"><i class="icon-ok icon-white"></i> Actions...</a>';
			
			$actions .= '<ul class="dropdown-menu">';
			
			$actions .= '<li><a href="'.site_url().'NetworkAdminPortal/EditCourseType/'.$type->ID.'.htm" class="alert-success"><i class="icon-pencil"></i> Edit Course Type</a></li>';
			$actions .= '<li><a href="'.site_url().'NetworkAdminPortal/DeleteCourseType/'.$type->ID.'.htm" class="alert-danger"><i class="icon-trash"></i> Delete Course Type</a></li>';
			
			$actions .= '</ul>';
			
			$actions .= '</div>';
			$actions .= '</div>';
			
			$this->table->add_row($type->type, $actions);
		}
		
		$data['table'] = $this->table->generate();
		
		$this->load->view('nwa/coursetypes/browsecoursetypes', $data);
		
		$this->load->view('templates/footer');
	}
	
	public function EditCourseType($id)
	{
		// load default stuff
		$data['title']	= 'Edit Course Type';
		$data['name']	= $this->admin->data['firstname'] . ' ' . $this->admin->data['lastname'];
		
		$this->load->view('templates/header', $data);
		$this->load->view('nwa/nav');
		
		$this->load->view('nwa/templates/heading', $data);
		
		// formy wormy
		$this->load->model(array('coursetypes'));
		$this->load->library('form_validation');
		
		// set validation rules
		$this->form_validation->set_rules('type', 'Course Type', 'required');
		$this->form_validation->set_rules('coursemarks', 'Course Marks', 'required');
		
		if($this->form_validation->run() == FALSE)
		{
			// only need one course, but the object is returned in an array
			$course = $this->coursetypes->get_course_type($id);
			$data['course'] = $course[0];
						
			$this->load->view('nwa/forms/editcoursetype', $data);
		}
		else
		{
			$type			= $this->input->post('type');
			$marks			= $this->input->post('coursemarks');
			
			if($this->coursetypes->update_course_type($id, $type, $marks))
			{
				$data['msg'] = 'The course type was successfully updated.';
				$this->load->view('nwa/templates/success', $data);
			}
			else
			{
				$this->load->view('nwa/templates/failure');
			}
		}
		
		$this->load->view('templates/footer');
	}
	
	public function DeleteCourseType($id)
	{
		// load default stuff
		$data['title']	= 'Delete Course Type';
		$data['name']	= $this->admin->data['firstname'] . ' ' . $this->admin->data['lastname'];
		
		$this->load->view('templates/header', $data);
		$this->load->view('nwa/nav');
		
		$this->load->view('nwa/templates/heading', $data);
		
		$this->load->model('coursetypes');
		
		if($this->coursetypes->delete_course_type($id))
		{
			$data['msg'] = 'The course type and associated data was successfully deleted from the system.';
			$this->load->view('nwa/templates/success', $data);
		}
		else
		{
			$this->load->view('nwa/templates/failure');
		}
		
		$this->load->view('templates/footer');
	}
	
	#########################################
	#	Module Management					#
	#########################################
	
	public function AddModule()
	{
		// load default stuff
		$data['title']	= 'Add Module';
		$data['name']	= $this->admin->data['firstname'] . ' ' . $this->admin->data['lastname'];
		
		$this->load->view('templates/header', $data);
		$this->load->view('nwa/nav');
		
		$this->load->view('nwa/templates/heading', $data);
		
		// formy wormy
		$this->load->model(array('Staff_UserDB', 'courses', 'modules'));
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('modname', 'Module Name', 'required');
		$this->form_validation->set_rules('taught_by', 'Lecturer', 'required');
		$this->form_validation->set_rules('course', 'Course', 'required');
		
		if($this->form_validation->run() == FALSE)
		{
			$data['courses']	= $this->courses->get_course_list();
			$data['staff']		= $this->Staff_UserDB->get_lecturers();
			
			$this->load->view('nwa/forms/addmodule.php', $data);
		}
		else
		{
			$name		= $this->input->post('modname');
			$taught_by	= $this->input->post('taught_by');
			$course		= $this->input->post('course');
			$year		= $this->input->post('year');
			
			if($this->modules->add_module($name, $taught_by, $course, $year))
			{
				$data['msg'] = "The module was successfully added to the system.";
				$this->load->view('nwa/templates/success', $data);
			}
			else
			{
				$this->load->view('nwa/templates/failure');	
			}
		}
	}
	
	public function BrowseModules()
	{
		// load default stuff
		$data['title']	= 'Browse Modules';
		$data['name']	= $this->admin->data['firstname'] . ' ' . $this->admin->data['lastname'];
		
		$this->load->view('templates/header', $data);
		$this->load->view('nwa/nav');
		
		$this->load->view('nwa/templates/heading', $data);
		
		// tabley stuff
		$this->load->model('modules');
		$this->load->library('table');
		$this->table->set_template(array('table_open' => '<table class="table table-striped table-bordered span6">'));
		$this->table->set_heading('Module Name', 'Taught By', 'Actions');
		
		foreach($this->modules->get_modules() as $module)
		{
			
			$actions  = '<div class="btn-toolbar">';
			$actions .= '<div class="btn-group">';
			
			$actions .= '<a href="#" class="btn btn-primary dropdown-toggle"><i class="icon-ok icon-white"></i> Actions...</a>';
			
			$actions .= '<ul class="dropdown-menu">';
			
			$actions .= '<li><a href="'.site_url().'NetworkAdminPortal/EditModule/'.$module['ID'].'.htm" class="alert-success"><i class="icon-pencil"></i> Edit Module</a></li>';
			$actions .= '<li><a href="'.site_url().'NetworkAdminPortal/DeleteModule/'.$module['ID'].'.htm" class="alert-danger"><i class="icon-trash"></i> Delete Module</a></li>';
			
			$actions .= '</ul>';
			
			$actions .= '</div>';
			$actions .= '</div>';
			
			$this->table->add_row($module['ModuleName'] . '<br />' . $module['Course'] . '; Year '. $module['year'], $module['Lecturer'], $actions);
		}
		
		$data['table'] = $this->table->generate();
		
		$this->load->view('nwa/modules/browsemodules', $data);
		
		$this->load->view('templates/footer');
	}
	
	public function DeleteModule($id)
	{
		// load default stuff
		$data['title']	= 'Delete Module';
		$data['name']	= $this->admin->data['firstname'] . ' ' . $this->admin->data['lastname'];
		
		$this->load->view('templates/header', $data);
		$this->load->view('nwa/nav');
		
		$this->load->view('nwa/templates/heading', $data);
		
		$this->load->model('modules');
		
		if($this->modules->delete_module($id))
		{
			$data['msg'] = 'The module and associated data was successfully deleted from the system.';
			$this->load->view('nwa/templates/success', $data);
		}
		else
		{
			$this->load->view('nwa/templates/failure');
		}
		
		$this->load->view('templates/footer');
	}
	
	public function EditModule($id)
	{
		// load default stuff
		$data['title']	= 'Edit Modules';
		$data['name']	= $this->admin->data['firstname'] . ' ' . $this->admin->data['lastname'];
		
		$this->load->view('templates/header', $data);
		$this->load->view('nwa/nav');

		$this->load->view('nwa/templates/heading', $data);
		
		// formy stuff
		$this->load->library('form_validation');
		$this->load->model(array('Staff_UserDB', 'courses', 'modules'));
		
		$this->form_validation->set_rules('modname', 'Module Name', 'required');
		$this->form_validation->set_rules('course', 'Course', 'required');
		$this->form_validation->set_rules('lecturer', 'Lecturer', 'required');
		
		if($this->form_validation->run() == FALSE)
		{
			$data['courselist'] = $this->courses->get_course_list();
			$data['staff']  = $this->Staff_UserDB->get_lecturers();
			$data['module']		= $this->modules->get_module($id);
			$data['current_course'] = $this->modules->get_module_course($id);
			
			$this->load->view('nwa/forms/editmodule.php', $data);
		}
		else
		{
			$module	  = $this->input->post('modname');
			$lecturer = $this->input->post('lecturer');
			$course	  = $this->input->post('course');
			
			if($this->modules->edit_module($id, $module, $lecturer, $course))
			{
				$data['msg'] = "The module was successfully updated.";
				$this->load->view('nwa/templates/success', $data);
			}
			else
			{
				$this->load->view('nwa/templates/failure');
			}
		}
		
		$this->load->view('templates/footer');
	}
	
	#########################################
	#	Department Management				#
	#########################################
	
	public function AddDepartment()
	{
		// load default stuff
		$data['title']	= 'Add Department';
		$data['name']	= $this->admin->data['firstname'] . ' ' . $this->admin->data['lastname'];
		
		$this->load->view('templates/header', $data);
		$this->load->view('nwa/nav');

		$this->load->view('nwa/templates/heading', $data);
		
		// formy stuff
		$this->load->library('form_validation');
		$this->load->model('departments');
		
		$this->form_validation->set_rules('department', 'Department Name', 'required');
		
		if($this->form_validation->run() == FALSE)
		{
			$this->load->view('nwa/forms/adddepartment.php', $data);
		}
		else
		{
			$dept	= $this->input->post('department');
			
			if($this->departments->add_department($dept))
			{
				$data['msg'] = "The department was successfully added.";
				$this->load->view('nwa/templates/success', $data);
			}
			else
			{
				$this->load->view('nwa/templates/failure');
			}
		}
		
		$this->load->view('templates/footer');	
	}
	
	public function BrowseDepartments()
	{
		// load default stuff
		$data['title']	= 'Browse Departments';
		$data['name']	= $this->admin->data['firstname'] . ' ' . $this->admin->data['lastname'];
		
		$this->load->view('templates/header', $data);
		$this->load->view('nwa/nav');
		
		$this->load->view('nwa/templates/heading', $data);
		
		// tabley stuff
		$this->load->model('departments');
		$this->load->library('table');
		$this->table->set_template(array('table_open' => '<table class="table table-striped table-bordered span6">'));
		$this->table->set_heading('Department Name', 'Actions');
		
		foreach($this->departments->get_departments() as $dept)
		{
			
			$actions  = '<div class="btn-toolbar">';
			$actions .= '<div class="btn-group">';
			
			$actions .= '<a href="#" class="btn btn-primary dropdown-toggle"><i class="icon-ok icon-white"></i> Actions...</a>';
			
			$actions .= '<ul class="dropdown-menu">';
			
			$actions .= '<li><a href="'.site_url().'NetworkAdminPortal/EditDepartment/'.$dept['ID'].'.htm" class="alert-success"><i class="icon-pencil"></i> Edit Department</a></li>';
			$actions .= '<li><a href="'.site_url().'NetworkAdminPortal/DeleteDepartment/'.$dept['ID'].'.htm" class="alert-danger"><i class="icon-trash"></i> Delete Department</a></li>';
			
			$actions .= '</ul>';
			
			$actions .= '</div>';
			$actions .= '</div>';
			
			$this->table->add_row($dept['name'], $actions);
		}
		
		$data['table'] = $this->table->generate();
		
		$this->load->view('nwa/departments/browsedepartments', $data);
		
		$this->load->view('templates/footer');
	}
	
	public function DeleteDepartment($id)
	{
		// load default stuff
		$data['title']	= 'Delete Department';
		$data['name']	= $this->admin->data['firstname'] . ' ' . $this->admin->data['lastname'];
		
		$this->load->view('templates/header', $data);
		$this->load->view('nwa/nav');
		
		$this->load->view('nwa/templates/heading', $data);
		
		$this->load->model('departments');
		
		if($this->departments->delete_department($id))
		{
			$data['msg'] = 'The department and associated data was successfully deleted from the system.';
			$this->load->view('nwa/templates/success', $data);
		}
		else
		{
			$this->load->view('nwa/templates/failure');
		}
		
		$this->load->view('templates/footer');
	}
	
	public function EditDepartment($id)
	{
		// load default stuff
		$data['title']	= 'Edit Department';
		$data['name']	= $this->admin->data['firstname'] . ' ' . $this->admin->data['lastname'];
		
		$this->load->view('templates/header', $data);
		$this->load->view('nwa/nav');

		$this->load->view('nwa/templates/heading', $data);
		
		// formy stuff
		$this->load->library('form_validation');
		$this->load->model(array('Staff_UserDB', 'departments'));
		
		$this->form_validation->set_rules('department', 'Department Name', 'required');
		
		if($this->form_validation->run() == FALSE)
		{
			$data['department'] = $this->departments->get_department($id);
			
			$this->load->view('nwa/forms/editdepartment.php', $data);
		}
		else
		{
			$name	  = $this->input->post('department');
			
			if($this->departments->edit_department($id, $name))
			{
				$data['msg'] = "The department was successfully updated.";
				$this->load->view('nwa/templates/success', $data);
			}
			else
			{
				$this->load->view('nwa/templates/failure');
			}
		}
		
		$this->load->view('templates/footer');
	}
	
	#########################################
	#	Staff Management					#
	#########################################
	
	public function AddStaff()
	{
		// load default stuff
		$data['title']	= 'Add Staff';
		$data['name']	= $this->admin->data['firstname'] . ' ' . $this->admin->data['lastname'];
		
		$this->load->view('templates/header', $data);
		$this->load->view('nwa/nav');

		$this->load->view('nwa/templates/heading', $data);
		
		// formy stuff
		$this->load->library('form_validation');
		$this->load->model(array('Staff_UserDB', 'departments'));
		
		$this->form_validation->set_rules('id', 'Staff ID', 'required|alpha_numeric|is_unique[tblStaff.staff_id]');
		$this->form_validation->set_rules('firstname', 'First Name', 'required|alpha_dash');
		$this->form_validation->set_rules('lastname', 'Last Name', 'required|alpha_dash');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('password2', 'Password Confirmation', 'required|matches[password]');
		$this->form_validation->set_rules('address', 'Address', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('contactnumber', 'Contact Number', 'required');
		$this->form_validation->set_rules('department', 'Department', 'required');
		
		if($this->form_validation->run() == FALSE)
		{
			$data['departments'] = $this->departments->get_departments();
			$this->load->view('nwa/forms/addstaff.php', $data);
		}
		else
		{
			$id		= $this->input->post('id');
			$fname	= $this->input->post('firstname');
			$lname	= $this->input->post('lastname');
			$pass	= $this->input->post('password');
			$addr	= $this->input->post('address');
			$email	= $this->input->post('email');
			$num	= $this->input->post('contactnumber');
			$dept	= $this->input->post('department');
			
			if($this->Staff_UserDB->add_staff($id, $fname, $lname, $pass, $addr, $email, $num, $dept))
			{
				$data['msg'] = "The staff member was successfully added.";
				$this->load->view('nwa/templates/success', $data);
			}
			else
			{
				$this->load->view('nwa/templates/failure');
			}
		}
		
		$this->load->view('templates/footer');	
	}
	
	public function BrowseStaff()
	{
		// load default stuff
		$data['title']	= 'Browse Staff';
		$data['name']	= $this->admin->data['firstname'] . ' ' . $this->admin->data['lastname'];
		
		$this->load->view('templates/header', $data);
		$this->load->view('nwa/nav');
		
		$this->load->view('nwa/templates/heading', $data);
		
		// tabley stuff
		$this->load->model(array('departments', 'Staff_UserDB'));
		$this->load->library('table');
		$this->table->set_template(array('table_open' => '<table class="table table-striped table-bordered span7">'));
		$this->table->set_heading('Name', 'Contact', 'Department', 'Actions');
		
		foreach($this->Staff_UserDB->get_lecturers_all_info() as $staff)
		{
			
			$actions  = '<div class="btn-toolbar">';
			$actions .= '<div class="btn-group">';
			
			$actions .= '<a href="#" class="btn btn-primary dropdown-toggle"><i class="icon-ok icon-white"></i> Actions...</a>';
			
			$actions .= '<ul class="dropdown-menu">';
			
			$actions .= '<li><a href="'.site_url().'NetworkAdminPortal/EditStaff/'.$staff->staff_id.'.htm" class="alert-success"><i class="icon-pencil"></i> Edit Staff</a></li>';
			$actions .= '<li><a href="'.site_url().'NetworkAdminPortal/DeleteStaff/'.$staff->staff_id.'.htm" class="alert-danger"><i class="icon-trash"></i> Delete Staff</a></li>';
			
			$actions .= '</ul>';
			
			$actions .= '</div>';
			$actions .= '</div>';
			
			$dept = $this->departments->get_department($staff->dept)->name;
			
			$this->table->add_row($staff->firstName . ' ' . $staff->lastName . '<br /><strong>' . $staff->staff_id . '</strong>', '<strong>Address</strong>:<br />' . $staff->address . '<br /><br />Phone: ' . $staff->contactNumber . '<br /><a href="mailto:'.$staff->email.'"><i class="icon-envelope"></i> '.$staff->email.'</a>', $dept, $actions);
		}
		
		$data['table'] = $this->table->generate();
		
		$this->load->view('nwa/staff/browsestaff', $data);
		
		$this->load->view('templates/footer');
	}
	
	public function EditStaff($id)
	{
		// load default stuff
		$data['title']	= 'Edit Staff';
		$data['name']	= $this->admin->data['firstname'] . ' ' . $this->admin->data['lastname'];
		
		$this->load->view('templates/header', $data);
		$this->load->view('nwa/nav');

		$this->load->view('nwa/templates/heading', $data);
		
		// formy stuff
		$this->load->library('form_validation');
		$this->load->model(array('Staff_UserDB', 'departments'));
		
		$this->form_validation->set_rules('firstname', 'First Name', 'required');
		$this->form_validation->set_rules('lastname', 'Last Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required');
		$this->form_validation->set_rules('contactnumber', 'Contact Number', 'required');
		$this->form_validation->set_rules('department', 'Department', 'required');
		
		if($this->form_validation->run() == FALSE)
		{
			$data['staff'] = $this->Staff_UserDB->get_lecturer($id);
			$data['departments'] = $this->departments->get_departments();
			
			$this->load->view('nwa/forms/editstaff.php', $data);
		}
		else
		{
			$fname	  = $this->input->post('firstname');
			$lname	  = $this->input->post('lastname');
			$pass	  = $this->input->post('password') == "" ? NULL : $this->input->post('password');
			$email	  = $this->input->post('email');
			$addr	  = $this->input->post('address');
			$num	  = $this->input->post('contactnumber');
			$dept	  = $this->input->post('department');
			
			if($this->Staff_UserDB->edit_staff($id, $fname, $lname, $pass, $addr, $email, $num, $dept))
			{
				$data['msg'] = "The staff member was successfully updated.";
				$this->load->view('nwa/templates/success', $data);
			}
			else
			{
				$this->load->view('nwa/templates/failure');
			}
		}
		
		$this->load->view('templates/footer');
	}
	
	public function DeleteStaff($id)
	{
		// load default stuff
		$data['title']	= 'Delete Staff Member';
		$data['name']	= $this->admin->data['firstname'] . ' ' . $this->admin->data['lastname'];
		
		$this->load->view('templates/header', $data);
		$this->load->view('nwa/nav');
		
		$this->load->view('nwa/templates/heading', $data);
		
		$this->load->model('Staff_UserDB');
		
		if($this->Staff_UserDB->delete_staff($id))
		{
			$data['msg'] = 'The staff member was successfully deleted from the system.';
			$this->load->view('nwa/templates/success', $data);
		}
		else
		{
			$data['msg'] = 'Are you sure all of this staff members modules and associated data has been moved?';
			$this->load->view('nwa/templates/failure', $data);
		}
		
		$this->load->view('templates/footer');
	}
}


/* End of File */
/* Location: ./application/controllers/NetworkAdminPortal */