<?php if( ! defined('BASEPATH') ) exit ('No direct script access!');

class StudentPortal extends CI_Controller
{
	private $student;
	
	public function __construct()
	{
		parent::__construct();
		// if user is not logged in, return to home!
		$this->load->library('StudentOperations');
		$this->load->helper('url');
		if($this->studentoperations->is_logged_in() == false)
			header('Location: '.site_url().'Home.htm');
			
		// Serialize the cookie and store as an object
		require_once(APPPATH.'libraries/Student.php');
		$this->student = unserialize(base64_decode($_COOKIE['userdata']));
	}
	
	public function index()
	{
		// set up some basic data to be loaded into views
		$data['title']	= "Student Portal";
		$data['name']	= ucfirst($this->student->data['firstname']) . ' ' . ucfirst($this->student->data['lastname']);
		$data['course']	= ucfirst($this->studentoperations->get_course_name($this->student->data['course']));
		$data['year']	= $this->student->data['year'];
		
		// start generating the module/assignment table
		// load table library provided by CodeIgniter
		$this->load->library('table');
		
		// set table caption
		$this->table->set_caption('My Modules');
		
		// set table headings
		$this->table->set_heading('Modules', 'Taught By', 'Assignments');
		
		// table template
		$this->table->set_template(array('table_open' => '<table class="table table-striped">'));
		
		$tabledata = $this->studentoperations->get_modules($this->student->data['id']);
		
		// create assignment button link drop down menu
		for($i = 0; $i < count($tabledata); $i++)
		{
			$link = '<div class="btn-toolbar">';
			$link .= '<div class="btn-group">';
			$link .= '<a href="#" class="btn btn-primary dropdown-toggle">Assignments&nbsp;';
			$link .= '<span class="caret"></span>';
			$link .= '</a>';
			$link .= '<ul class="dropdown-menu">';
			
			$assignments = $this->studentoperations->get_assignments($this->student->data['course'], $tabledata[$i]['ID']);
			
			// if there are no assignments set yet, inform student of that
			if(count($assignments) == 0)
			{
				$link .= '<li>No assignments have been listed for this module at this time.</li>';
			}
			// other wise, fill 'er up!
			else
			{
				for($x = 0; $x < count($assignments); $x++)
				{
					// Outputs links like: /StudentPortal/AssignmentInfo/COURSE_ID/MODULE_ID/ASSIGNMENT_ID.htm
					$link .= '<li><a href="./StudentPortal/AssignmentInfo/'.$this->student->data['course'].'/'.$tabledata[$i]['ID'].'/'.$assignments[$x]['ID'].'.htm">'.$assignments[$x]['name'].'<br />'.$assignments[$x]['duedate'].'</a></li>';
				}
			}
			
			$link .= '</ul>';
			$link .= '</div>';
			$link .= '</div>';
			
			
			$this->table->add_row($tabledata[$i]['modName'], $tabledata[$i]['name'], $link);
		}
		
		$data['table'] = $this->table->generate();
				
		$this->load->view('templates/header', $data);
		
		$this->load->view('student/templates/heading', $data);
		$this->load->view('student/portal', $data);
		
		$this->load->view('templates/footer');
	}
	
	public function AssignmentInfo($cid, $mid, $aid)
	{
		// load form & validation classes
		$this->load->library(array('form_validation', 'upload'));
		$this->load->helper(array('form'));

		// set some upload data
		$config['upload_path'] = './uploads/students';
		// there's a bad bug which means .docx files amongst others won't be seen as "valid" using CI's MIME checking, so we'll do it ourselves
		// and tell CI to allow all files
		$config['allowed_types'] = '*';
		
		
		$this->upload->initialize($config);
		
		// set some validation rules
		$this->form_validation->set_rules('disclaimer', 'Disclaimer', 'required');
		$this->form_validation->set_rules('assid', 'Assignment ID', 'required|callback__valid_ID');
		
		if($this->form_validation->run() == false)
		{
			$data['title'] 	= 'Assignment Information';
			$data['name']	= ucfirst($this->student->data['firstname']) . ' ' . ucfirst($this->student->data['lastname']);
			$data['course']	= ucfirst($this->studentoperations->get_course_name($this->student->data['course']));
			$data['year']	= $this->student->data['year'];
			$data['assid']	= $aid;
			
			// get assignment brief info
			$aBrief = $this->studentoperations->get_assignment_brief($aid);
			
			$data['assignmenttitle']	= $aBrief['name'];
			$data['lecturer']			= $aBrief['lecturer'];
			$data['date']				= $aBrief['duedate'];
			$data['formats']			= str_replace(',', ', ', $aBrief['formats']);
			$data['specification']		= $aBrief['specification_file'];
			
			
			$this->load->view('templates/header', $data);
			
			$this->load->view('student/templates/heading', $data);
			
			// load brief
			$this->load->view('student/assignmentbrief', $data);
			
			// if current revision is final, disable upload form
			if($this->studentoperations->is_current_revision_final($this->student, $aid))
				$data['disable_form'] = true;
			else
				$data['disable_form'] = false;
			
			// load upload form
			$this->load->view('student/forms/upload', $data);
			
			// load current revision form
			$data['current_revision'] = $this->studentoperations->get_current_revision($this->student, $aid);			
			$this->load->view('student/forms/current_revision', $data);
			
			$this->load->view('templates/footer');
		}
		elseif($this->upload->do_upload('assignment'))
		{
			$data['title'] = 'Assignment Upload';
			$data['name']	= ucfirst($this->student->data['firstname']) . ' ' . ucfirst($this->student->data['lastname']);
			$data['course']	= ucfirst($this->studentoperations->get_course_name($this->student->data['course']));
			$data['year']	= $this->student->data['year'];
			
			$this->load->view('templates/header', $data);
			$this->load->view('student/templates/heading', $data);
			
			// upload succeeded, test the file for errors
			if($this->_virusscan($this->upload->data() == 1))
			{
				$data['error'] = 'virus';
				
				$this->load->view('student/error', $data);
			}
			// invalid extension
			else if( ! $this->_is_valid_ext($this->upload->data(), $aid))
			{
				$data['error'] = 'ext';
				
				$this->load->view('student/error', $data);
			}
			// everything is valid
			else
			{
				$file = $this->_rename_file($this->upload->data());
				if($this->studentoperations->submit_assignment($this->student, $file))
				{
					$this->load->view('student/uploadsuccess');
				}
			}
			
			$this->load->view('templates/footer');

		}
		else
		{
			echo $this->upload->display_errors();
			$data = $this->upload->data();
			echo $data['file_type'].'<br>';
			echo $_FILES['assignment']['type'];
		}
	}
	
	public function MyMarks()
	{
		$data['title'] = 'My Marks';
		$data['name']	= ucfirst($this->student->data['firstname']) . ' ' . ucfirst($this->student->data['lastname']);
		$data['course']	= ucfirst($this->studentoperations->get_course_name($this->student->data['course']));
		$data['year']	= $this->student->data['year'];
		
		$this->load->view('templates/header',$data);
		$this->load->view('student/templates/heading',$data);
		
		// create the marks table
		$this->load->library('table');
		$this->table->set_heading('Assignment Name', 'Module Name', 'Grade', 'Feedback');
		$this->table->set_template(array('table_open' => '<table class="table table-striped table-bordered span8">'));
		
		$marks = $this->studentoperations->get_marks($this->student->data['id']);
		
		if($marks == NULL)
		{
			$this->table->add_row(array('data' => 'No marks are available for any of your assignments yet.', 'colspan' => 4));
		}
		else
		{
			foreach($marks as $m)
			{
				$grade = $m->grade == NULL ? 'N/A' : $m->grade;
				$feedback = $m->feedback == NULL ? 'N/A' : $m->feedback;
				
				$this->table->add_row($m->AssignmentName, $m->ModuleName, $grade, $feedback);
			}
		}
		
		$data['table'] = $this->table->generate();
		
		$this->load->view('student/marks',$data);
		
		$this->load->view('templates/footer');
		
	}
	
	#########################################
	#	Scan file for viruses				#
	#	private								#
	#########################################
	
	private function _virusscan($file)
	{
		// executes virus scan on file, and removes if it detects a positive
		// outputs return value into $int
		exec('clamdscan '.$file['full_path'] . ' --remove', $out, $int);
		return $int;		
	}
	
	#########################################
	#	Is file valid ext?					#
	#	private								#
	#########################################
	
	private function _is_valid_ext($file, $aid)
	{
		$types = $this->studentoperations->get_valid_file_types($aid);
		// put the file types into an array
		$types = explode(",", $types);
		
		// get file extension
		$ext	= explode('.', $file['file_name']);
		// make $ext the last element of $ext
		$ext	= $ext[count($ext) - 1];
		
		$valid = false;
		
		foreach($types as $value)
		{
			// if the extension matches a valid one, set $valid to true and exit loop
			if($ext == $value)
			{
				$valid = true;
				break;	
			}
		}
		
		// if file is invalid, remove it
		if($valid == false)
		{
			exec('rm ' . $file['full_path']);
		}
			
		return $valid;
	}
	
	#########################################
	#	Make sure assignment ID is valid	#
	#	and nobody is submitting bad data!	#
	#	private								#
	#########################################
	
	public function _valid_id($aid)
	{
		return $this->studentoperations->is_valid_assignment_id($aid);
	}
	
	#########################################
	#	rename the file so that other files #
	#	with the same uploaded name don't	#
	#	overwrite it						#
	#	private								#
	#########################################
	
	private function _rename_file($file)
	{
		$filepath	= $file['file_path'];
		$filename	= $file['file_name'];
		
		// get file extension
		$ext		= explode('.', $filename);
		$ext		= $ext[count($ext) - 1];
		
		$salt		= rand(100, 20000);
		$newname	= $this->student->data['id'] . sha1($filename) . $salt . '.' . $ext;
		
		// rename the file using ubuntu shell
		shell_exec('mv ' . $filepath . $filename . ' ' . $filepath . $newname);
		
		// return path and name
		return $filepath . $newname;
	}
}

/* End of File */
/* Location: ./application/controllers/StudentPortal.php */